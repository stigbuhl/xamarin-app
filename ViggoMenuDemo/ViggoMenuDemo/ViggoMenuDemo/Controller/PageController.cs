﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ViggoMenuDemo.Controller
{
    public sealed class PageController
    {
        private static PageController instance = null;
        private static readonly object padlock = new object();

        MainPage mainPage;

        PageController()
        {
        }

        public static PageController Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new PageController();
                    }
                    return instance;
                }
            }
        }

        //Navigate to another contentpage
        public async void ChangeDetailPage(ContentPage contentPage)
        {
            NavigationPage.SetHasBackButton(contentPage, false);
            await mainPage.Detail.Navigation.PushAsync(contentPage);            
        }

        //Sets mainpage
        public void SetMainPage()
        {
            mainPage = new MainPage();
            App.Current.MainPage = mainPage;            
        }

        public void IsPresented(bool isPresented)
        {
            mainPage.IsPresented = isPresented;
        }
    }
}
