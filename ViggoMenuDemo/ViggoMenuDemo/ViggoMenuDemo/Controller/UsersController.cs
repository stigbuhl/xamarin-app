﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Controller
{
    public sealed class UsersController
    {
        private static UsersController instance = null;
        private static readonly object padlock = new object();

        UsersController()
        {
        }

        public static UsersController Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new UsersController();
                    }
                    return instance;
                }
            }
        }

        private async Task<string> RestContact(string address)
        {
            HttpResponseMessage response;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri($"http://{InfoController.Instance.GetDomain()}/rest/");

                response = await client.GetAsync(address);
            }

            return response.Content.ReadAsStringAsync().Result;
        }

        //Fetches userlist from usertypeId
        public async Task<List<UserDTO>> GetUserListFromId(int userTypeId)
        {
            string response = await RestContact($"users/list/{userTypeId}?token={InfoController.Instance.GetToken()}");

            JArray userArray = JArray.Parse(response);

            List<UserDTO> users = new List<UserDTO>();

            foreach (var item in userArray)
            {
                users.Add(item.ToObject<UserDTO>());
            }

            users = users.OrderBy(o => o.FullName).ToList();

            return users;
        }

        //Fetched specific user from userId
        public async Task<UserDTO> GetUserFromId(int listId, int userId)
        {
            UserDTO user = new UserDTO();

            foreach (var item in await GetUserListFromId(listId))
            {
                if (item.Id == userId)
                {
                    user = item;
                }
            }

            return user;
        }
    }
}
