﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Diary;
using ViggoMenuDemo.Model.User.Post;


namespace ViggoMenuDemo.Controller
{
    public sealed class DiaryController
    {
        private static DiaryController instance = null;
        private static readonly object padlock = new object();

        DiaryController()
        {
        }

        public static DiaryController Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DiaryController();
                    }
                    return instance;
                }
            }
        }
        
        //Establishes contach with server and returns result
        private async Task<string> RestContact(string address)
        {
            HttpResponseMessage response;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri($"http://{InfoController.Instance.GetDomain()}/rest/");

                response = await client.GetAsync(address);
            }

            return response.Content.ReadAsStringAsync().Result;
        }

        //Fetches diary categories
        public async Task<List<DiaryCategoryDTO>> GetCategories()
        {
            string response = await RestContact($"diary/{1}?token={InfoController.Instance.GetToken()}");

            JArray categoryArray = JArray.Parse(response);

            List<DiaryCategoryDTO> categories = new List<DiaryCategoryDTO>();

            foreach (var item in categoryArray)
            {
                categories.Add(item.ToObject<DiaryCategoryDTO>());
            }

            return categories;
        }

        //Fetches diaries
        public async Task<List<DiaryDTO>> GetDiaryList(DateTime date)
        {
            List<DiaryDTO> diary = new List<DiaryDTO>();

            string s = $"diary?{date.ToString("yyyy-MM-dd")}&token={InfoController.Instance.GetToken()}";

            string response = await RestContact($"diary?{date.ToString("yyyy-MM-dd")}&token={InfoController.Instance.GetToken()}");
            
            #region Testdata
            //diary.Add(new DiaryDTO
            //{
            //    UpdatedbyUser = await UsersController.Instance.GetUserFromId(2, InfoController.Instance.GetUserId()),
            //    Date = DateTime.Now,
            //    Headline = "Hej",
            //    Category = new List<DiaryCategoryDTO>
            //    {
            //        new DiaryCategoryDTO
            //        {
            //            Name = "Positivt"
            //        }
            //    },
            //    Contents = "Test content",
            //    Users = new List<Model.User.UserDTO>
            //    {
            //        await UsersController.Instance.GetUserFromId(1, 580),
            //    }
            //});

            //diary.Add(new DiaryDTO
            //{
            //    UpdatedbyUser = await UsersController.Instance.GetUserFromId(2, InfoController.Instance.GetUserId()),
            //    Date = DateTime.Now,
            //    Headline = "Hej",
            //    Category = new List<DiaryCategoryDTO>(),
            //    Contents = "Test content",
            //    Users = new List<Model.User.UserDTO>
            //    {
            //        await UsersController.Instance.GetUserFromId(1, 580),
            //    }
            //});

            //diary.Add(new DiaryDTO
            //{
            //    UpdatedbyUser = await UsersController.Instance.GetUserFromId(2, InfoController.Instance.GetUserId()),
            //    Date = DateTime.Now,
            //    Headline = "Hej",
            //    Category = new List<DiaryCategoryDTO>(),
            //    Contents = "Test content",
            //    Users = new List<Model.User.UserDTO>
            //    {
            //        await UsersController.Instance.GetUserFromId(1, 580),
            //    }
            //});

            //diary.Add(new DiaryDTO
            //{
            //    UpdatedbyUser = await UsersController.Instance.GetUserFromId(2, InfoController.Instance.GetUserId()),
            //    Date = DateTime.Now,
            //    Headline = "Hej",
            //    Category = new List<DiaryCategoryDTO>(),
            //    Contents = "Test content",
            //    Users = new List<Model.User.UserDTO>
            //    {
            //        await UsersController.Instance.GetUserFromId(1, 580),
            //    }
            //});

            //PostDiary(new DiaryDTO
            //{
            //    UpdatedbyUser = await UsersController.Instance.GetUserFromId(2, InfoController.Instance.GetUserId()),
            //    Date = DateTime.Now,
            //    Headline = "Hej",
            //    Category = new List<DiaryCategoryDTO>(),
            //    Contents = "Test content",
            //    //Users = new List<Model.User.UserDTO>
            //    //{
            //    //    await UsersController.Instance.GetUserFromId(1, 580),
            //    //},
            //    UsersIds = new List<int>
            //    {
            //        580,
            //    }
            //});

            #endregion

            return diary;
        }

        //public async void Comment(int id, PostAnswerDTO dto)
        //{
        //    HttpResponseMessage httpResponse;

        //    using (var client = new HttpClient())
        //    {
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        string address = $"http://{InfoController.Instance.GetDomain()}/rest/diary?token={InfoController.Instance.GetToken()}";

        //        HttpContent newContent = new FormUrlEncodedContent(new[]
        //        {
        //            new KeyValuePair<string, string>("diary", id.ToString()),
        //            new KeyValuePair<string, string>("diary", JsonConvert.SerializeObject(dto)),
        //        });

        //        httpResponse = await client.PostAsync(address, newContent);

        //        Debug.WriteLine($"SVAR: {httpResponse.Content.ReadAsStringAsync().Result}");
        //    }
        //}

        //Posts diary object
        public async void PostDiary(DiaryDTO diary)
        {
            //int response = 0;
            HttpResponseMessage httpResponse;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string address = $"http://{InfoController.Instance.GetDomain()}/rest/diary?token={InfoController.Instance.GetToken()}";

                HttpContent newContent = new FormUrlEncodedContent(new[]
                {
                        new KeyValuePair<string, string>("diary", JsonConvert.SerializeObject(diary)),
                });

                httpResponse = await client.PostAsync(address, newContent);

                Debug.WriteLine($"SVAR: {httpResponse.Content.ReadAsStringAsync().Result}");
            }

            //return response;
        }

        //Posts diary comment
        public async Task<int> PostComment(PostAnswerDTO postAnswer)
        {
            int response = 0;
            HttpResponseMessage httpResponse;

            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string address = $"http://{InfoController.Instance.GetDomain()}/rest/diary?token={InfoController.Instance.GetToken()}";

                    HttpContent newContent = new FormUrlEncodedContent(new[]
                    {
                    new KeyValuePair<string, string>("comment", JsonConvert.SerializeObject(postAnswer)),
                });

                    httpResponse = await client.PutAsync(address, newContent);

                    Debug.WriteLine($"SVAR: {httpResponse.Content.ReadAsStringAsync().Result}");

                    response = 1;
                }
            }
            catch
            {
                response = -1;
            }

            return response;
        }
    }
}
