﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViggoMenuDemo.Controller
{
    public sealed class InfoController
    {
        private static InfoController instance = null;
        private static readonly object padlock = new object();

        InfoController()
        {
        }

        public static InfoController Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new InfoController();
                    }
                    return instance;
                }
            }
        }

        //Sets login information if it's not already available
        public int LoginTrailer(string input)
        {
            int response = 0;

            JObject json = JObject.Parse(input);

            try
            {
                if (json["Token"] != null)
                {
                    response = (SetToken((string)json["Token"]) == 1) ? 1 : -1;
                }

                if (json["UserId"] != null)
                {
                    response = (SetUserId((int)json["UserId"]) == 1) ? 1 : -1;
                }

                if (json["MenuItems"] != null)
                {
                    response = (MenuController.Instance.BuildMenu(json["MenuItems"]) == 1) ? 1 : -1;
                }

                if (json["ClientId"] != null)
                {
                    response = (SetClientId((int)json["ClientId"]) == 1) ? 1 : -1;
                }
            }
            catch
            {
                response = -1;
            }

            return response;
        }

        //Sets token
        private int SetToken(string token)
        {
            int response = 0;

            try
            {
                App.Current.Properties["Token"] = token;
                response = 1;
            }
            catch
            {
                response = -1;
            }

            return response;
        }

        //Sets domain
        internal int SetDomain(string domain)
        {
            int response = 0;

            try
            {
                App.Current.Properties["Domain"] = domain;
                response = 1;
            }
            catch
            {
                response = -1;
            }

            return response;
        }

        //Sets User Id
        private int SetUserId(int userId)
        {
            int response = 0;

            try
            {
                App.Current.Properties["UserId"] = userId;
                response = 1;
            }
            catch
            {
                response = -1;
            }

            return response;
        }

        //Sets ClientId
        private int SetClientId(int clientId)
        {
            int response = 0;

            try
            {
                App.Current.Properties["ClientId"] = clientId;
                response = 1;
            }
            catch
            {
                response = -1;
            }

            return response;
        }

        //Sets username
        public int SetUserName(string userName)
        {
            int response = 0;

            try
            {
                App.Current.Properties["UserName"] = userName;
                response = 1;
            }
            catch
            {
                response = -1;
            }

            return response;
        }

        //Sets password
        public int SetPassword(string password)
        {
            int response = 0;

            try
            {
                App.Current.Properties["Password"] = password;
                response = 1;
            }
            catch
            {
                response = -1;
            }

            return response;
        }

        //Gets token from app properties
        public string GetToken()
        {
            if (App.Current.Properties["Token"] != null)
            {
                return (string)App.Current.Properties["Token"];
            }
            else
            {
                return "-1";
            }
        }

        //Fetches UserId, but does nu ask if UserID's not defined 
        public int GetUserId()
        {
            if (App.Current.Properties["UserId"] != null)
            {
                return (int)App.Current.Properties["UserId"];
            }
            else
            {
                return -1;
            }
        }

        //Fetches domain
        public string GetDomain()
        {
            if (App.Current.Properties["Domain"] != null)
            {
                return (string)App.Current.Properties["Domain"];
            }
            else
            {
                return "-1";
            }
        }

        //Fetches domain
        public string GetUserName()
        {
            if (App.Current.Properties["UserName"] != null)
            {
                return (string)App.Current.Properties["UserName"];
            }
            else
            {
                return "-1";
            }
        }

        //Fetches password
        public string GetPassword()
        {
            if (App.Current.Properties["Password"] != null)
            {
                return (string)App.Current.Properties["Password"];
            }
            else
            {
                return "-1";
            }
        }

        //Checks if logininfo is available
        public int HasLoginInfo()
        {
            int response = 0;

            if (GetDomain() != "-1" && GetUserName() != "-1" && GetPassword() != "-1")
            {
                response = 1;
            }
            else
            {
                response = -1;
            }

            return response;
        }
    }
}
