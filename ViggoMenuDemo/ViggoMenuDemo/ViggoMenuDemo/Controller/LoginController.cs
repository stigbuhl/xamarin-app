﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ViggoMenuDemo.Controller
{
    public sealed class LoginController
    {
        private static LoginController instance = null;
        private static readonly object padlock = new object();

        LoginController()
        {

        }

        public static LoginController Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new LoginController();
                    }
                    return instance;
                }
            }
        }

        //Posts username + password and gets login information
        public async Task<int> Login(string domain, string username, string password)
        {
            int response = 0;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string address = $"http://{domain}/rest/auth";

                //Values to post to server
                HttpContent newContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", username),
                    new KeyValuePair<string, string>("password", password)
                });

                HttpResponseMessage httpResponse = await client.PostAsync(address, newContent);

                Debug.WriteLine("Hello");

                try
                {

                    if (InfoController.Instance.LoginTrailer(httpResponse.Content.ReadAsStringAsync().Result) == 1) // Sets login information to app properties
                    {                        
                        if (InfoController.Instance.SetDomain(domain) == 1 && InfoController.Instance.SetPassword(password) == 1 && InfoController.Instance.SetUserName(username) == 1)
                        {
                            response = 1;
                        }
                        else
                        {
                            response = -1;
                        }                        
                    }
                    else
                    {
                        response = -1;
                    }
                }
                catch
                {
                    response = -1;
                }            
            }

            return response;        
        }        
    }    
}
