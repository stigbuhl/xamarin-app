﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.DetailPages;
using ViggoMenuDemo.DetailPages.Admin;
using ViggoMenuDemo.DetailPages.Calendar;
using ViggoMenuDemo.DetailPages.Files;
using ViggoMenuDemo.DetailPages.Functions;
using ViggoMenuDemo.DetailPages.Messages;
using ViggoMenuDemo.DetailPages.RoomsPages;
using ViggoMenuDemo.DetailPages.Users;
using ViggoMenuDemo.Menu;
using ViggoMenuDemo.Menu.MenuItems;
using Xamarin.Forms;

namespace ViggoMenuDemo.Controller
{
    class MenuController
    {
        private static MenuController instance = null;
        private static readonly object padlock = new object();

        MenuPage menuPage;

        List<Menu.MenuItems.MenuItem> buttons;

        MenuController()
        {
        }

        public static MenuController Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new MenuController();
                    }
                    return instance;
                }
            }
        }

        //Builds menu
        public int BuildMenu(JToken menuItems)
        {
            int response = 0;

            try
            {
                buttons = new List<Menu.MenuItems.MenuItem>();
                foreach (var item in menuItems)
                {
                    //If item contains subitems, add subitems to menu
                    if (item["SubItems"] != null && item["SubItems"].HasValues)
                    {
                        Menu.MenuItems.MenuItem newMenuItem = new Menu.MenuItems.MenuItem
                        {
                            Text = (string)item["Name"],
                            SubItems = new List<SubItem>(),
                            Image = new FileImageSource { File = GetImage((string)item["Id"]) },
                            BackgroundColor = Color.Transparent,
                            HorizontalOptions = LayoutOptions.StartAndExpand
                        };
                        foreach (var subItem in item["SubItems"])
                        {
                            newMenuItem.SubItems.Add(new SubItem
                            {
                                Text = (string)subItem["Name"],
                                Command = new Command(o => {
                                    PageController.Instance.ChangeDetailPage(GetSubItemPage(subItem));
                                    PageController.Instance.IsPresented(false);
                                }),
                                BackgroundColor = Color.Transparent,
                                HorizontalOptions = LayoutOptions.StartAndExpand
                            });
                        }
                        //Command for toggling visibility
                        newMenuItem.Command = new Command(o =>
                        {
                            foreach (var subItem in newMenuItem.SubItems)
                            {
                                subItem.IsVisible = (subItem.IsVisible) ? subItem.IsVisible = false : subItem.IsVisible = true;
                            }
                        });
                        buttons.Add(newMenuItem);
                    }
                    else
                    {                        
                        buttons.Add(new Menu.MenuItems.MenuItem
                        {
                            Text = (string)item["Name"],
                            Command = new Command(o => {
                                //Command for page navigation
                                PageController.Instance.ChangeDetailPage(GetItemPage(item));
                                PageController.Instance.IsPresented(false);
                                }),
                            Image = new FileImageSource { File = GetImage((string)item["Id"]) },
                            BackgroundColor = Color.Transparent,
                            HorizontalOptions = LayoutOptions.StartAndExpand
                        });
                    }
                }
                response = 1;
            }
            catch
            {
                response = -1;
            }                    

            return response;
        }

        //Returns menulist
        public List<Menu.MenuItems.MenuItem> GetMenu()
        {
            if (buttons != null)
            {
                return buttons;
            }
            else
            {
                return null;
            }
        }

        //Sets and returns new menupage
        public ContentPage GetMenuPage()
        {
            menuPage = new MenuPage();
            return menuPage;
        }

        //Determines which image to use in menu
        private FileImageSource GetImage(string id)
        {
            switch (id)
            {
                case "nav_start":
                    return "play.png";
                case "nav_calendar":
                    return "calendar.png";
                case "nav_users":
                    return "users.png";
                case "nav_rooms":
                    return "discussion_double.png";
                case "nav_files":
                    return "documents.png";
                case "nav_messages":
                    return "mail.png";
                case "nav_functions":
                    return "functions.png";
                case "nav_admin":
                    return "admin.png";
                default:
                    break;
            }
            return null;
        }

        //Determines which contentpage specific menupages point to 
        private ContentPage GetItemPage(JToken item)
        {
            switch ((string)item["Id"])
            {
                case "nav_start":
                    return new StartPage();
                //case "nav_calendar":
                //    return GetCalendarPage(item);
                //case "nav_users":
                //    return GetUserPage(item);
                case "nav_rooms":
                    return new RoomsPage();
                //case "nav_files":
                //    return GetFilesPage(item);
                case "nav_messages":
                    return new MessagesPage();
                //case "nav_functions":
                //    return GetFunctionsPage(item);
                //case "nav_admin":
                //    return GetAdminPage(item);
                default:
                    return new ErrorPage();
            }
        }

        //Sets which contentpage subitem should point to
        private ContentPage GetSubItemPage(JToken item)
        {
            switch ((string)item["Controller"])
            {
                case "Calendar":
                    return new Calendar();
                case "Diary":
                    return new DiaryPage();
                case "Guard":
                    return new Guard();
                case "Homework":
                    return new Homework();
                case "Premises":
                    return new Premises();
                case "Absence":
                    return new Absence();
                case "Groups":
                    return new Groups();
                case "Injury":
                    return new Injury();
                case "Relations":
                    return new Relations();
                case "Users":
                    return new UsersPage(item);
                case "File":
                    return new FilesPage();
                case "Print":
                    return new Print();
                case "Evaluation":
                    return new Evaluation();
                case "Formular":
                    return new Formular();
                case "CheckIn":
                    return new CheckIn();
                case "Archive":
                    return new Archive();
                case "System":
                    return new SystemPage();
                case "Access":
                    return new Access();
                case "AdminUsers":
                    return new AdminUsers();
                case "Administrator":
                    return new Administrator();
                default:
                    return new ErrorPage();
            }
        }

        #region NotUsed
        private ContentPage GetCalendarPage(JToken item)
        {
            switch ((string)item["Controller"])
            {
                case "Calendar":
                    return new Calendar();
                case "Diary":
                    return new DiaryPage();
                case "Guard":
                    return new Guard();
                case "Homework":
                    return new Homework();
                case "Premises":
                    return new Premises();
                default:
                    return new ErrorPage();
            }
        }

        private ContentPage GetUserPage(JToken item)
        {
            switch ((string)item["Controller"])
            {
                case "Absence":
                    return new Absence();
                case "Groups":
                    return new Groups();
                case "Injury":
                    return new Injury();
                case "Relations":
                    return new Relations();
                case "Users":
                    return new UsersPage(item);
                default:
                    return new ErrorPage();
            }
        }

        private ContentPage GetFilesPage(JToken item)
        {
            switch ((string)item["Controller"])
            {
                case "File":
                    return new FilesPage();
                default:
                    return new ErrorPage();
            }
        }

        private ContentPage GetFunctionsPage(JToken item)
        {
            switch ((string)item["Controller"])
            {
                case "Print":
                    return new Print();
                case "Evaluation":
                    return new Evaluation();
                case "Formular":
                    return new Formular();
                case "CheckIn":
                    return new CheckIn();
                case "Archive":
                    return new Archive();
                default:
                    return new ErrorPage();
            }
        }

        private ContentPage GetAdminPage(JToken item)
        {
            switch ((string)item["Controller"])
            {
                case "System":
                    return new SystemPage();
                case "Access":
                    return new Access();
                case "AdminUsers":
                    return new AdminUsers();
                case "Administrator":
                    return new Administrator();
                default:
                    return new ErrorPage();
            }
        }
        #endregion
    }
}
