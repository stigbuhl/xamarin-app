﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Bulletin;
using ViggoMenuDemo.Model.Diary;
using ViggoMenuDemo.Model.Home;
using ViggoMenuDemo.Model.Message;
using ViggoMenuDemo.Model.Room;

namespace ViggoMenuDemo.Controller
{
    public sealed class HomeController
    {
        private static HomeController instance = null;
        private static readonly object padlock = new object();
                        
        HomeController()
        {
        }

        public static HomeController Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new HomeController();
                    }
                    return instance;
                }
            }
        }

        private async Task<string> RestContact(string address)
        {
            HttpResponseMessage response;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri($"http://{InfoController.Instance.GetDomain()}/rest/");

                response = await client.GetAsync(address);
            }
            
            return response.Content.ReadAsStringAsync().Result;

        }

        //Gets HomePage
        public async Task<JArray> ReadHomePage()
        {
            return JArray.Parse(await RestContact($"home/?token={InfoController.Instance.GetToken()}"));
        }

        //Fetches messages
        public async Task<List<MessageDTO>> ReadMessages(int show)
        {
            List<MessageDTO> messages = new List<MessageDTO>();

            string response = await RestContact($"home/message/{show}?token={InfoController.Instance.GetToken()}");

            if (response != "null")
            {
                //Response made into JArray
                JArray messageArray = JArray.Parse(response);

                foreach (var item in messageArray)
                {
                    //Object from JArray made into MessageDTO object
                    messages.Add(item.ToObject<MessageDTO>());
                }
            }            

            return messages;
        }

        //Fetches absences
        public async Task<HomeAbsenceDTO> ReadAbsence(int show)
        {
            string response = await RestContact($"home/absence/{show}?token={InfoController.Instance.GetToken()}");

            JObject jobject = JObject.Parse(response);

            HomeAbsenceDTO homeabsence = jobject.ToObject<HomeAbsenceDTO>();

            return homeabsence;
        }

        //Fetches diaries
        public async Task<List<DiaryDTO>> ReadDiary(int show)
        {
            string response = await RestContact($"home/diary/{show}?token={InfoController.Instance.GetToken()}");

            JArray diaryArray = JArray.Parse(response);

            List<DiaryDTO> diaries = new List<DiaryDTO>();

            foreach (var item in diaryArray)
            {
                diaries.Add(item.ToObject<DiaryDTO>());
            }

            return diaries;
        }

        //Fetches rooms
        public async Task<List<RoomHomeDTO>> ReadRooms(int show)
        {
            string response = await RestContact($"home/rooms/{show}?token={InfoController.Instance.GetToken()}");

            JObject roomObject = JObject.Parse(response);

            JArray roomsArray = (JArray)roomObject["RoomsList"];

            List<RoomHomeDTO> rooms = new List<RoomHomeDTO>();

            foreach (var item in roomsArray)
            {
                rooms.Add(item.ToObject<RoomHomeDTO>());
            }

            return rooms;
        }

        //Fetches bulletins
        public async Task<BulletinBoardDTO> ReadBulletin(int userType, int show)
        {
            string response = await RestContact($"home/bulletin/{userType}?show={show}&token={InfoController.Instance.GetToken()}");

            BulletinBoardDTO bulletinBoard = JObject.Parse(response).ToObject<BulletinBoardDTO>();

            return bulletinBoard; 
        }

        //Fetches Relations
        public async Task<List<HomeRelationsDTO>> ReadRelations(int show)
        {
            string response = await RestContact($"home/relations/{show}?token={InfoController.Instance.GetToken()}");

            JArray array = JArray.Parse(response);

            List<HomeRelationsDTO> homeRelations = new List<HomeRelationsDTO>();

            foreach (var item in array)
            {
                homeRelations.Add(item.ToObject<HomeRelationsDTO>());
            }

            return homeRelations;
        }

        //Fetches schedule - does not work!
        public async Task<string> ReadSchedule(int show)
        {
            return await RestContact($"home/schedule/{show}?token={InfoController.Instance.GetToken()}");
        }
    }
}
