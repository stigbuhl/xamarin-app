﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Message;

namespace ViggoMenuDemo.Controller
{
    public sealed class MessageController
    {
        private static MessageController instance = null;
        private static readonly object padlock = new object();

        MessageController()
        {
        }

        public static MessageController Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new MessageController();
                        }
                        
                    }
                }
                return instance;
            }
        }

        //Establishes contach with server and returns result
        private async Task<string> RestContact(string address)
        {
            HttpResponseMessage response;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri($"http://{InfoController.Instance.GetDomain()}/rest/");

                response = await client.GetAsync(address);
            }

            return response.Content.ReadAsStringAsync().Result;
        }

        public async Task<MessageDTO> GetMessageById(MessageDTO message)
        {
            #region MyRegion
            //HttpResponseMessage response;

            //using (var client = new HttpClient())
            //{
            //    client.DefaultRequestHeaders.Accept.Clear();
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    string dest = $"http://{InfoController.Instance.GetDomain()}/rest/messages/get/{message.Id}?token={InfoController.Instance.GetToken()}";

            //    response = await client.GetAsync(dest);

            //} 
            #endregion

            string response = await RestContact($"messages/get/{message.Id}?token={InfoController.Instance.GetToken()}");

            JObject messageInput = JObject.Parse(response);

            message = messageInput.ToObject<MessageDTO>();

            if (message.Answers != null)
            {
                foreach (var item in message.Answers)
                {
                    message.Answers.Add(item);
                }
            }

            return message;
        }

        public MessageDTO MakeMessage(JObject input)
        {
            MessageDTO message = input.ToObject<MessageDTO>();

            return message;
        }

        public async Task<int> SendMessage(string subject, string body, string to, string answerId)
        {
            int methodResponse = 0;

            HttpResponseMessage response;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string address = $"http://{InfoController.Instance.GetDomain()}/rest/messages?token={InfoController.Instance.GetToken()}";
                
                HttpContent newContent;
                if (answerId == null)
                {
                    newContent = new FormUrlEncodedContent(new[]
                    {
                            new KeyValuePair<string, string>("subject", subject),
                            new KeyValuePair<string, string>("body", body),
                            new KeyValuePair<string, string>("to", to),
                        });
                }
                else
                {
                    newContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("subject", subject),
                            new KeyValuePair<string, string>("body", body),
                            new KeyValuePair<string, string>("to", to),
                            new KeyValuePair<string, string>("answerid", answerId)
                        });
                }
                response = await client.PostAsync(address, newContent);

                methodResponse = 1;

                Debug.WriteLine(response.Content.ReadAsStringAsync().Result);
            }

            return methodResponse;
        }

        public async Task<List<MessageDTO>> GetMessages()
        {
            List<MessageDTO> mail = new List<MessageDTO>();
            try
            {
                #region MyRegion
                //HttpResponseMessage response;

                //using (var client = new HttpClient())
                //{
                //    client.DefaultRequestHeaders.Accept.Clear();
                //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //    string dest = $"http://{InfoController.Instance.GetDomain()}/rest/messages?token={InfoController.Instance.GetToken()}";

                //    response = await client.GetAsync(dest);
                //} 
                #endregion

                string response = await RestContact($"messages?token={InfoController.Instance.GetToken()}");

                try
                {
                    JArray responseArray = JArray.Parse(response);

                    foreach (var message in responseArray)
                    {
                        try
                        {
                            mail.Add(message.ToObject<MessageDTO>());
                        }
                        catch
                        {

                        }
                    }
                }
                catch
                {

                }
            }
            catch
            {

            }

            return mail;
        }

        public async Task DeleteMessage(int messageId)
        {
            #region MyRegion
            //using (var client = new HttpClient())
            //{
            //    client.DefaultRequestHeaders.Accept.Clear();
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    string address = $"http://{InfoController.Instance.GetDomain()}/rest/messages/delete/{messageId}? token={InfoController.Instance.GetToken()}";               
            //} 
            #endregion

            await RestContact($"messages/delete/{messageId}? token={InfoController.Instance.GetToken()}");
        }
    }
}
