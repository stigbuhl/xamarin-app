﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using ViggoMenuDemo.Controller;
using Xamarin.Forms;

namespace ViggoMenuDemo
{
    public class LoginPage : ContentPage
    {
        public LoginPage()
        {
            var domainEntry = new Entry { Placeholder = "Domæne", Text = "192.168.15.92:8080" };
            var userNameEntry = new Entry { Placeholder = "Brugernavn", Text = "mp@edb-brugs.dk" };
            var passwordEntry = new Entry { Placeholder = "Password", IsPassword = true, Text = "poulsen" };
            
            StackLayout stackLayout = new StackLayout
            {
                Padding = new Thickness(20, 20, 20, 20),
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Orientation = StackOrientation.Vertical,
                Spacing = 15,
                Children = {
                        domainEntry,
                        userNameEntry,
                        passwordEntry,

                        new Button
                        {
                            BackgroundColor = Color.Silver,
                            Text = "Login", Command = new Command(async o=>
                            {
                                try
                                {
                                    //Sends inputdata to logincontroller 
                                    int response = await LoginController.Instance.Login(domainEntry.Text, userNameEntry.Text, passwordEntry.Text);
                                    if (response == 1)
                                    {
                                        //If login succeeds, MainPage is set
                                        PageController.Instance.SetMainPage();
                                    }
                                }
                                catch
                                {
                                    await DisplayAlert("Fejl", "Der skete en fejl under login, prøv igen", "OK");
                                }
                            })
                        }
                    }
            };

            Grid grid = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(1.0, GridUnitType.Star) },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(1.0, GridUnitType.Star) },
                }
            };

            Image image = new Image();
            image.Source = "background1.jpg";
            image.Aspect = Aspect.Fill;
            
            if (Device.OS == TargetPlatform.iOS)
            {
                stackLayout.Padding = new Thickness(20, 40, 20, 20);
                grid.Padding = new Thickness(0, 40, 0, 0);
            }

            ScrollView scrollView = new ScrollView
            {
                Content = stackLayout,
            };

            //ScrollView is is placed on top of image
            grid.Children.Add(image, 0, 0);
            grid.Children.Add(scrollView, 0, 0);

            Content = grid;
        }
    }
}
