﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using ViggoMenuDemo.Model.User;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.UsersPage
{
    public class UserInfoViewCell : ViewCell
    {
        public UserInfoViewCell(UserDTO user)
        {
            View = new StackLayout
            {
                Orientation = StackOrientation.Vertical,

                Children = {
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,

                        Children =
                        {
                            new Label
                            {
                                TextColor = Color.Silver,
                                Text = "Medlem af: "
                            },
                            new Label
                            {
                                Text = "Test"
                            }
                        }
                    },
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,

                        Children =
                        {
                            new Label
                            {
                                TextColor = Color.Silver,
                                Text = "Køn: "
                            },
                            new Label
                            {
                                Text = "Test"
                            }
                        }
                    }
                }
            };
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
        }
    }
}
