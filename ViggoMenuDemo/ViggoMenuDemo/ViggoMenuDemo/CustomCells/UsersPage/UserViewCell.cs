﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.UsersPage
{
    public class UserViewCell : ViewCell
    {
        Image image = null;
        Label userName = null;

        public UserViewCell()
        {
            image = new Image
            {
                HeightRequest = 35,
                WidthRequest = 35,
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            userName = new Label
            {
                VerticalOptions = LayoutOptions.Center,
            };

            StackLayout stackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,

                Children =
                {
                    image,
                    userName,
                }
            };

            View = stackLayout;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            var item = BindingContext as UserDTO;

            if (item != null)
            {
                image.Source = new UriImageSource
                {
                    Uri = new Uri(item.ImageBig),
                };

                userName.Text = item.FullName;
            }
        }
    }
}
