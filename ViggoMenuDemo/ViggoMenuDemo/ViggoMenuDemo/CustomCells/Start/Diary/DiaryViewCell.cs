﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Diary;
using ViggoMenuDemo.Model.User;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.Start
{
    class DiaryViewCell : ViewCell
    {
        Image image = null;
        Label sender_lbl = null;
        Label sentDate_lbl = null;
        Label subject_lbl = null;
        Label category_lbl = null;
        Label content_lbl = null;

        ObservableCollection<UserDTO> users = new ObservableCollection<UserDTO>();

        StackLayout usersStackLayout = null;

        public DiaryViewCell()
        {
            Grid grid = new Grid
            {
                ColumnSpacing = 2,
                Padding = 5,
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = 40 },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = 40 }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) }
                }
            };

            image = new Image
            {
                HeightRequest = 35,
                WidthRequest = 35,
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            sender_lbl = new Label();

            sentDate_lbl = new Label
            {
                TextColor = Color.Silver,
            };

            subject_lbl = new Label
            {
                FontAttributes = FontAttributes.Bold,
            };

            category_lbl = new Label
            {
                TextColor = Color.Silver,
            };

            content_lbl = new Label();

            StackLayout imageStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.StartAndExpand,
                HorizontalOptions = LayoutOptions.Center,

                Children =
                {
                    image,
                },
            };

            StackLayout senderStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.StartAndExpand,

                Children =
                {
                    sender_lbl,
                    sentDate_lbl,
                },
            };

            StackLayout subjectStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.StartAndExpand,

                Children =
                {
                    subject_lbl,
                    category_lbl
                },
            };

            usersStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                VerticalOptions = LayoutOptions.StartAndExpand
            };            

            grid.Children.Add(imageStackLayout, 0, 0);
            Grid.SetRowSpan(imageStackLayout, 4);

            grid.Children.Add(senderStackLayout, 1, 0);
            grid.Children.Add(subjectStackLayout, 1, 1);
            grid.Children.Add(content_lbl, 1, 2);
            grid.Children.Add(usersStackLayout, 1, 3);

            View = grid;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            var item = BindingContext as DiaryDTO;
            if (item != null)
            {
                image.Source = new UriImageSource
                {
                    Uri = new Uri(item.UpdatedbyUser.ImageBig),
                };

                sender_lbl.Text = item.UpdatedbyUser.FullName;

                sentDate_lbl.Text = item.Date.ToString();

                subject_lbl.Text = item.Headline;

                if (item.Category.Count != 0)
                {
                    category_lbl.Text += "(";
                    foreach (var category in item.Category)
                    {
                        if (category == item.Category.First())
                            category_lbl.Text += $"{category.Name}";
                        else
                            category_lbl.Text += $" {category.Name}";

                        if (category != item.Category.Last())
                            category_lbl.Text += ",";
                    }
                    category_lbl.Text += ")";
                }

                content_lbl.Text = item.Contents;

                

                if (item.Users != null)
                {
                    int counter = 0;

                    foreach (var user in item.Users)
                    {
                        if (counter < 4)
                        {
                            usersStackLayout.Children.Add(DiaryView(user));
                        }
                        counter++;
                    }

                    if (counter > 1)
                    {
                        usersStackLayout.Children.Add(new Label
                        {
                            VerticalOptions = LayoutOptions.Center,
                            FontAttributes = FontAttributes.Bold,
                            Text = $"Plus {counter - 4} andre",
                        });
                    }
                }
                

                
            }
        }

        private StackLayout DiaryView(UserDTO user)
        {
            StackLayout stacklayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.Center,

                Children =
                    {
                        new Image
                        {
                            HeightRequest = 35,
                            WidthRequest = 35,
                            Aspect = Aspect.AspectFill,
                            HorizontalOptions = LayoutOptions.Center,
                            VerticalOptions = LayoutOptions.Center,
                            Source = user.ImageBig,
                        },
                        new Label
                        {
                            Text = user.FullName,
                            VerticalOptions = LayoutOptions.Center,
                        },
                    }
            };

            return stacklayout;
        }
    }
}
