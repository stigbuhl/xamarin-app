﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.Start
{
    public class DiaryUserViewCell : ViewCell
    {
        Image image = null;
        Label fullname_lbl = null;
        StackLayout stackLayout = null;

        public DiaryUserViewCell()
        {
            image = new Image
            {

            };

            fullname_lbl = new Label
            {

            };

            stackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,

                Children =
                {
                    image,
                    fullname_lbl,
                },
            };

            View = stackLayout;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            var user = BindingContext as UserDTO;

            if (user != null)
            {
                image.Source = new Uri(user.ImageBig);

                fullname_lbl.Text = user.FullName;
            }
        }
    }
}
