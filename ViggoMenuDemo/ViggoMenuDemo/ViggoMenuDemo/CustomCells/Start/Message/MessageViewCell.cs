﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
//using ImageCircle.Forms.Plugin.Abstractions;
using ViggoMenuDemo.Model.Message;

namespace ViggoMenuDemo.CustomCells.Start
{
    public class MessageViewCell : ViewCell
    {
        Image image = null;

        public MessageViewCell()
        {
            var grid = new Grid
            {
                ColumnSpacing = 2,
                Padding = 5,
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = 40 },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = 40 }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                }
            };

            image = new Image
            {
                HeightRequest = 35,
                WidthRequest = 35,
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            Frame contentFrame = new Frame
            {
                OutlineColor = Color.Transparent,
                HasShadow = false,
                BackgroundColor = Color.FromHex("#03A9F4"),
                VerticalOptions = LayoutOptions.Center
            };

            Label fromUser_lbl = new Label
            {
                VerticalOptions = LayoutOptions.Center,
            };

            Label date_lbl = new Label
            {
                VerticalOptions = LayoutOptions.Center,
            };

            Label headline_lbl = new Label
            {
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.Bold,
            };

            Label content_lbl = new Label
            {
                VerticalOptions = LayoutOptions.Center,
                TextColor = Color.Gray,
            };

            fromUser_lbl.SetBinding(Label.TextProperty, "FullName");
            date_lbl.SetBinding(Label.TextProperty, "Id");
            headline_lbl.SetBinding(Label.TextProperty, "Headline");
            content_lbl.SetBinding(Label.TextProperty, "Contents");

            contentFrame.Content = content_lbl;

            StackLayout stackLayout = new StackLayout
            {
                Margin = 0,
                Spacing = 0,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,

                Children =
                {
                    image,
                },
            };

            grid.Children.Add(stackLayout, 0, 0);
            Grid.SetRowSpan(stackLayout, 3);



            grid.Children.Add(new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    fromUser_lbl,
                    date_lbl,
                }
            }, 1, 0);
            grid.Children.Add(headline_lbl, 1, 1);
            grid.Children.Add(contentFrame, 1, 2);

            View = grid;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            var item = BindingContext as MessageDTO;
            if (item != null)
            {
                image.Source = new UriImageSource
                {
                    Uri = new Uri(item.ImageSmall),
                    CachingEnabled = false,
                };
                    
            }
        }
    }
}
