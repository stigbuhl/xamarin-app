﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.Start.Absence
{
    class AbsenceViewCell : ViewCell
    {
        Label cause_lbl = null;
        Label user_lbl = null;


        public AbsenceViewCell()
        {
            Grid grid = new Grid
            {
                ColumnSpacing = 2,
                Padding = 5,
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                }                
            };

            cause_lbl = new Label
            {

            };

            user_lbl = new Label
            {

            };

            StackLayout stackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,

                Children =
                {
                    //new Label { },
                    cause_lbl,
                    user_lbl
                }
            };

            grid.Children.Add(stackLayout);

            View = grid;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            var item = BindingContext as AbsenceDTO;

            if (item != null)
            {
                cause_lbl.Text = item.AbsenceCause.Cause;
                user_lbl.Text = item.User.FullName;
            }
        }
    }
}
