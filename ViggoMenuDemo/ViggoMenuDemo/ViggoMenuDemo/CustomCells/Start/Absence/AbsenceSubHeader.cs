﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Absence;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.Start
{
    class AbsenceSubHeader : ViewCell
    {
        //Image image = null;
        Label subHeaderText_lbl = null;

        public AbsenceSubHeader()
        {
            //image = new Image
            //{

            //};

            subHeaderText_lbl = new Label
            {
                FontSize = 20,
            };

            StackLayout stackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,

                Children =
                {
                    subHeaderText_lbl,
                }
            };

            View = stackLayout;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            var item = BindingContext as AbsencePlaceDTO;

            if (item != null)
            {
                //image.Source = new UriImageSource
                //{

                //};

                subHeaderText_lbl.Text = item.Place.ToUpper();
            }
        }
    }
}
