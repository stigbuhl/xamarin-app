﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.DetailPages.Start;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.Start
{
    class GroupHeaderViewCell : ViewCell
    {
        Image image = null;
        public GroupHeaderViewCell()
        {
            
            Label headerLabel = new Label
            {
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.Bold,
            };

            headerLabel.SetBinding(Label.TextProperty, "LongName");

            image = new Image
            {
                VerticalOptions = LayoutOptions.Center,
            };

            //Height = 120;            

            headerLabel.FontSize = 30;

            var stackLayout = new StackLayout
            {                
                Margin = 0,
                Spacing = 0,
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Transparent,
                HeightRequest = 120,

                Children =
                {
                    image,
                    headerLabel,
                },
            };

            if (Device.OS == TargetPlatform.iOS)
            {
                stackLayout.BackgroundColor = Color.White;
            }

            View = stackLayout;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            var item = BindingContext as GroupedStartModel;
            if (item != null)
            {
                image.Source = new UriImageSource
                {
                    Uri = new Uri(item.Url),

                    //Gemmer billede til en anden god gang, er default 'true'
                    //CachingEnabled = true,
                    //Definerer hvor lang tid billedet er gemt
                    //CacheValidity = new TimeSpan(5, 0, 0, 0)
                };

            }
        }
    }
}
