﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Controller;
using ViggoMenuDemo.Model.Room;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.Start.Rooms
{
    public class RoomViewCell : ViewCell
    {
        Image senderImage = null;
        Image iconImage = null;
        Label user_lbl = null;
        Label date_lbl = null;
        Label roomName_lbl = null;
        Label subject_lbl = null;
        Label content_lbl = null;

        public RoomViewCell()
        {
            Grid grid = new Grid
            {
                ColumnSpacing = 2,
                Padding = 5,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = 40 },
                    new ColumnDefinition { Width = 40 },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) }
                }
            };

            senderImage = new Image
            {
                HeightRequest = 35,
                WidthRequest = 35,
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            iconImage = new Image
            {
                HeightRequest = 35,
                WidthRequest = 35,
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            user_lbl = new Label();

            date_lbl = new Label
            {
                TextColor = Color.Silver,
            };

            roomName_lbl = new Label
            {
                FontAttributes = FontAttributes.Bold,
            };

            subject_lbl = new Label();

            content_lbl = new Label
            {
                TextColor = Color.Silver,
            };

            StackLayout imageStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.StartAndExpand,
                HorizontalOptions = LayoutOptions.Center,

                Children =
                {
                    senderImage,
                }
            };

            StackLayout senderDateStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.StartAndExpand,

                Children =
                {
                    user_lbl,
                    date_lbl
                }
            };

            StackLayout iconStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.StartAndExpand,
                HorizontalOptions = LayoutOptions.Center,

                Children =
                {
                    iconImage,
                }
            };

            grid.Children.Add(imageStackLayout, 0, 0);
            Grid.SetRowSpan(imageStackLayout, 4);

            grid.Children.Add(senderDateStackLayout, 1, 0);
            Grid.SetColumnSpan(senderDateStackLayout, 2);

            grid.Children.Add(iconStackLayout, 1, 1);
            Grid.SetRowSpan(iconStackLayout, 3);

            grid.Children.Add(roomName_lbl, 2, 1);
            grid.Children.Add(subject_lbl, 2, 2);
            grid.Children.Add(content_lbl, 2, 3);

            View = grid;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            var item = BindingContext as RoomHomeDTO;

            if (item != null)
            {
                senderImage.Source = new UriImageSource
                {
                    Uri = new Uri(item.UpdatedByUser.ImageBig),
                };

                iconImage.Source = new UriImageSource
                {
                    Uri = new Uri($"http://{InfoController.Instance.GetDomain()}/Content/Icons/{item.Icon}"),
                };

                user_lbl.Text = item.UpdatedByUser.FullName;

                date_lbl.Text = item.Timestamp.ToString();

                roomName_lbl.Text = item.RoomName;

                subject_lbl.Text = item.Headline;

                content_lbl.Text = item.Contents;
            }
        }
    }
}
