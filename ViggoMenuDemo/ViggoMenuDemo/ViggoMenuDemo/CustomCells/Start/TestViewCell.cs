﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.Start
{
    class TestViewCell : ViewCell
    {
        public TestViewCell()
        {
            //Label functionLabel = new Label();
            //functionLabel.SetBinding(Label.TextProperty, "Functionid");

            //functionLabel.BackgroundColor = Color.Blue;

            Label textLabel = new Label();
            textLabel.SetBinding(Label.TextProperty, "Text");

            var stackLayout = new StackLayout
            {
                VerticalOptions = LayoutOptions.Center,
                Spacing = 0,
                Children =
                            {
                                //functionLabel,
                                textLabel,
                            },
            };

            View = stackLayout;
        }

    }
}
