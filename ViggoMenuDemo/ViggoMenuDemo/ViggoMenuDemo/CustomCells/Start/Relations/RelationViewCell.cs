﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Home;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.Start.Relations
{
    class RelationViewCell : ViewCell
    {
        Image userImage = null;
        Label userName_lbl = null;

        StackLayout stackLayout = null;

        public RelationViewCell()
        {
            var grid = new Grid
            {
                ColumnSpacing = 2,
                Padding = 5,
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = 40 },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = 40 }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                }
            };

            userImage = new Image
            {
                HeightRequest = 35,
                WidthRequest = 35,
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            userName_lbl = new Label
            {

            };

            stackLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical
            };            

            grid.Children.Add(new StackLayout {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Start,

                Children =
                {
                    userImage
                }
            }, 0, 0);
            Grid.SetRowSpan(userImage, 2);

            grid.Children.Add(userName_lbl, 1, 0);

            grid.Children.Add(stackLayout, 1, 1);

            View = grid;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            var item = BindingContext as HomeRelationsDTO;

            if (item != null)
            {
                userImage.Source = new UriImageSource
                {
                    Uri = new Uri(item.User.ImageBig),
                };

                userName_lbl.Text = item.User.FullName;

                #region Absence

                if (item.Absence != null)
                {
                    if (item.Absence.Count > 0)
                    {
                        StackLayout absenceStackLayout = new StackLayout
                        {
                            Orientation = StackOrientation.Vertical
                        };
                        stackLayout.Children.Add(new StackLayout
                        {
                            Children =
                        {
                            new Label
                            {
                                Text = "Fravær",
                                FontSize = 20,
                            },
                            absenceStackLayout
                        },
                        });

                        foreach (var absence in item.Absence)
                        {
                            absenceStackLayout.Children.Add(new StackLayout
                            {
                                Orientation = StackOrientation.Horizontal,

                                Children =
                            {
                                new Label
                                {
                                    Text = absence.AbsenceCause.Cause
                                },
                                new Label
                                {
                                    Text = $"({absence.AbsencePlace.Place})",
                                    TextColor = Color.Silver,
                                },
                                new Label
                                {
                                    Text = $"{absence.Timestart.ToString()} - {absence.Timeend.ToString()}" ,
                                }
                            }
                            });
                        }
                    }                    
                }
                
                #endregion

                #region Diary

                if (item.Diary != null)
                {
                    if (item.Diary.Count > 0)
                    {
                        StackLayout diaryStackLayout = new StackLayout
                        {
                            Orientation = StackOrientation.Vertical
                        };
                        stackLayout.Children.Add(new StackLayout
                        {
                            Children =
                        {
                            new Label
                            {
                                Text = "Dagbog",
                                FontSize = 20,
                            },
                            diaryStackLayout
                        },
                        });

                        foreach (var diary in item.Diary)
                        {
                            string info = "";
                            foreach (var category in diary.Category)
                            {
                                info += category.Name;

                                if (diary.Category.Last() != category)
                                {
                                    info += ", ";
                                }
                            }

                            Label category_lbl = new Label
                            {

                            };

                            if (info != "")
                            {
                                category_lbl.Text = $"({info})";
                            }

                            diaryStackLayout.Children.Add(new StackLayout
                            {
                                Orientation = StackOrientation.Horizontal,

                                Children =
                            {
                                new Label
                                {
                                    Text = $"{diary.Date.ToString("dddd d MMMM", CultureInfo.CurrentCulture)}",
                                    TextColor = Color.Silver
                                },
                                category_lbl
                            }
                            });
                        }
                    }                    
                }

                #endregion
            };
        }
    }
}
