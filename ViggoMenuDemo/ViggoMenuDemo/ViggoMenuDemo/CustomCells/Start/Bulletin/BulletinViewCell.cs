﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Bulletin;
using Xamarin.Forms;

namespace ViggoMenuDemo.CustomCells.Start.Bulletin
{
    class BulletinViewCell : ViewCell
    {
        Image userImage = null;
        Label userName_lbl = null;
        Label date_lbl = null;
        Label subject_lbl = null;
        Label content_lbl = null;

        public BulletinViewCell()
        {
            Grid grid = new Grid
            {
                ColumnSpacing = 2,
                Padding = 5,
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = 40 },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = 40 },
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) },
                }
            };

            userImage = new Image
            {
                HeightRequest = 35,
                WidthRequest = 35,
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            userName_lbl = new Label
            {

            };

            date_lbl = new Label
            {
                TextColor = Color.Silver,
            };

            subject_lbl = new Label
            {
                FontSize = 20,
                FontAttributes = FontAttributes.Bold,
            };

            content_lbl = new Label
            {

            };

            StackLayout imageStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.StartAndExpand,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Transparent,

                Children =
                {
                    userImage,
                }
            };

            StackLayout userNameStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.StartAndExpand,

                Children =
                {
                    userName_lbl,
                    date_lbl
                },
            };

            Frame contentFrame = new Frame
            {
                HasShadow = true,
                BackgroundColor = Color.FromHex("#faf6ca"),
                OutlineColor = Color.Transparent,
                VerticalOptions = LayoutOptions.Center,

                Content = new StackLayout
                {
                    Orientation = StackOrientation.Vertical,
                    VerticalOptions = LayoutOptions.StartAndExpand,
                    HorizontalOptions = LayoutOptions.StartAndExpand,

                    Children =
                    {
                        subject_lbl,
                        content_lbl
                    },
                }
            };

            grid.Children.Add(imageStackLayout, 0, 0);
            Grid.SetRowSpan(imageStackLayout, 2);

            grid.Children.Add(userNameStackLayout, 1, 0);
            grid.Children.Add(contentFrame, 1, 1);

            View = grid;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            var item = BindingContext as BulletinDTO;

            userImage.Source = new UriImageSource
            {
                Uri = new Uri(item.UpdatedbyUser.ImageBig),
            };

            userName_lbl.Text = item.UpdatedbyUser.FullName;

            date_lbl.Text = item.Timestamp.ToString();

            subject_lbl.Text = item.Headline;

            content_lbl.Text = item.Contents;
        }
    }
}
