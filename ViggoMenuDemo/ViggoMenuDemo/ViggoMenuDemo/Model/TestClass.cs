﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Interfaces;

namespace ViggoMenuDemo.Model
{
    public class TestClass : IListableObject
    {
        public string Text { get; set; }
        public int OrderBy { get; set; }
        public int Side { get; set; }
        public int Functionid { get; set; }
        public int Show { get; set; }
        public int UserType { get; set; }

        public TestClass()
        {

        }

        public TestClass(JToken item)
        {
            Text = item.ToString();
            OrderBy = (int)item["OrderBy"];
            Side = (int)item["Side"];
            Functionid = (int)item["Functionid"];
            Show = (int)item["Show"];
            if (!IsNullOrEmpty(item["UserType"]))
            {
                UserType = (int)item["UserType"];
            }
        }

        public static bool IsNullOrEmpty(JToken token)
        {
            return (token == null) ||
                   (token.Type == JTokenType.Array && !token.HasValues) ||
                   (token.Type == JTokenType.Object && !token.HasValues) ||
                   (token.Type == JTokenType.String && token.ToString() == String.Empty) ||
                   (token.Type == JTokenType.Null);
        }
    }
}
