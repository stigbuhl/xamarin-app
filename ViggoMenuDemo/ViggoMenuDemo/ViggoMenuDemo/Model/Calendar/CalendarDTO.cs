﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Homework;
using ViggoMenuDemo.Model.Injury;
using ViggoMenuDemo.Model.SchedulePlanning.Elements;
using ViggoMenuDemo.Model.SchedulePlanning.Schedule;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Calendar
{
    public class CalendarDTO
    {
        private DateTime _StartDateTime {get;set;}
        private DateTime _EndDateTime { get; set; }
        public int Id { get; set; }
        public string Headline { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public DateTime StartDateTime {
            get
            {
                if (Premises == null || Premises.Count ==0)
                {
                    return _StartDateTime;
                }
                return Premises.Min(t => t.TimeStart);
            }
            set
            {
                _StartDateTime = value;
            }
        }

        public DateTime EndDateTime
        {
            get
            {
                if (Premises == null || Premises.Count == 0)
                {
                    return _EndDateTime;
                }
                return Premises.Max(t => t.TimeEnd);
            }
            set
            {
                _EndDateTime = value;
            }
        }

        public List<PremisesDTO> Premises { get; set; }
        public string PlaceName { get; set; }
        public CalendarAddEditVisibility Visibility { get; set; }
        public List<ScheduleDTO> Schedules { get; set; }
        public List<GroupDTO> Groups { get; set; }
        public List<UserSubstitute> UsersWithSubstitutes { get; set; }
        
        public List<HomeworkDTO> Homeworks { get; set; }
        public bool IsAssignment { get; set; }
        public bool IsGuard { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsSerie { get; set; }
        public bool ShowOnInfoscreen { get; set; }
        public bool ShowOnHomepage { get; set; }

        public int? SerieId { get; set; }
        public int MadeBy { get; set; }
        public DateTime MadeDate { get; set; }
        public DateTime Timestamp { get; set; }
        public string Classes { get; set; }
        public int? ElementId { get; set; }
        public ScheduleElementDTO Element { get; set; }
        public int? ScheduleTemplatesWeekId { get; set; }
    }
    public class dbCalenderRelations
    {
        public int UserId { get; set; }
        public int CalendarId { get; set; }
        public string CalendarName { get; set; }
        public string CalendarDescription { get; set; }
        public string CalendarNote { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }
        public int? PremisesId { get; set; }
        public string PremisesName { get; set; }
        public int? HomeworkId { get; set; }
        public string HomeworHeadline { get; set; }
        public string HomeworDescription { get; set; }
        public bool IsPrivate { get; set; }
    }

    public class CalendarUserSettings
    {
        public int UserId { get; set; }
        public bool ShowSpecial { get; set; }
        public bool ShowGuard { get; set; }
        public ShowSchedulesDTO showSchedules { get; set; }
    }
    public class GetCalendarSettings
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; } 
        public bool AccessHomework { get; set; } 
        public bool ShowNote { get; set; }
        public bool ShowPrivate { get; set; }
        public bool ShowNoSchedule { get; set; }
        public bool ShowSpecial { get; set; }
        public bool ShowGuard { get; set; }
        public bool ShowPersonalGuard { get; set; }
        public bool BirthDay { get; set; }
        public int HowMany { get; set; }
        public int? SkipHowMany { get; set; }
        public bool ShowNoGroupsOrUsers { get; set; }
        public List<CalendarUserSettings> UserSettings { get; set; }
        public List<int> UserIds { get; set; }
        public List<int> ScheduleIds { get; set; }
        public List<int> ExcludeScheduleIds { get; set; }
        public List<int> ExcludeElementsIds { get; set; }
        public List<int> PremisesIds { get; set; }
        public ShowSchedulesDTO ShowSchedules { get; set; }
        
    }
    public class CalendarItemDTO
    {
        public int id { get; set; }
        public int calendarId { get; set; }
        public string name { get; set; }
        public string hintClass { get; set; }
        public string contents { get; set; }
        public string note { get; set; }
        public string premises { get; set; }
        public DateTime timestart { get; set; }
        public DateTime timeend { get; set; }
        public int minutes { get; set; }
        public string borderColor { get; set; }
        public string backgroundColor { get; set; }
        public string color { get; set; }
        public string type { get; set; } //guard or birthday
        public bool readOnly { get; set; }
        public string classes { get; set; }
        public CalendarItemLinksDTO links { get; set; }
        public UserDTO UpdatedByUser { get; set; }
        public DateTime Timestamp { get; set; }
        public DateTime MadeTimestamp { get; set; }
        public int? MadeBy { get; set; }
        public string printcontents { get; set; }
        public string printnote { get; set; }
        public string elementType { get; set; }
    }

    
    public class CalendarItemLinksDTO
    {
        public string show { get; set; }
        public string edit { get; set; }
        public string copy { get; set; }
        public string swap { get; set; }
        public string delete { get; set; }
        public string dblclick { get; set; }
        public bool AccessAllElement { get; set; }
        public List<ScheduleElementDTO> AccessElement { get; set; } 
    }
    public class CalendarLinksDTO
    {
        public string add { get; set; }
        public string addprivate { get; set; }
        public string addscheduleandtime { get; set; }
        public string move { get; set; }
        public string copy { get; set; }
        public string delete { get; set; }
        public string execute { get; set; }
        public string getItems { get; set; }

    }
    public class AddEditCalendarDTO
    {
        public int Id { get; set; }
        public string Headline { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public bool AccessToNote { get; set; }
        public bool Private { get; set; }
        public string Place { get; set; }
        public bool Special { get; set; }
        public bool MadeInViggo { get; set; }

        public List<PremisesDTO> Premises { get; set; }
        public bool Serie { get; set; }
        public int? SerieId { get; set; }
        public List<CalendarAddEditDTO> SerieCalendar { get; set; }
        public string SerieIds { get; set; }
        public CalendarAddEditSerie SerieInfo { get; set; }
        public CalendarAddEditVisibility Visibility { get; set; }
        public List<ScheduleDTO> Schedules { get; set; }
        public List<PremisesDTO> SerieList { get; set; }
        public List<int> SchedulesIds { get; set; }
        public List<int> GroupsIds { get; set; }
        public List<GroupDTO> Groups { get; set; }
        public List<int> UsersIds { get; set; }
        public List<UserDTO> Users { get; set; }
        public DateTime Timestamp { get; set; }
        public DateTime MadeTime { get; set; }
        public int? MadeBy { get; set; }
        public DateTime Timestart { get; set; }
        public DateTime TimeEnd { get; set; }
        public UserDTO UpdatedByUser { get; set; }
        public string GroupsNamesString { get; set; }
        
        public string PremisesNamsString { get; set; }

    }
    public class CalendarEditDTO
    {
        public CalendarDTO Calendar { get; set; }
        public List<CalendarAddEditDTO> SerieCalendar { get; set; }
        public List<ScheduleDTO> AllSchedule { get; set; }
        public CalendarAddEditSerie CalenderSerieInfo{ get; set; }
        public bool AccessToNote { get; set; }
        public bool AccessToEdit { get; set; }
    }
    public class CalendarAddEditDTO
    {
        private DateTime _StartDateTime { get; set; }
        private DateTime _EndDateTime { get; set; }
        public int Id { get; set; }
        public string Headline { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public bool Private { get; set; }
        public string Place { get; set; }
        public bool Special { get; set; }
        public List<PremisesDTO> Premises { get; set; }
        public bool Serie { get; set; }
        public List<CalendarAddEditDTO> SerieCalendar { get; set; }
        public string SerieIds { get; set; }
        public CalendarAddEditSerie SerieInfo { get; set; }
        public CalendarAddEditVisibility Visibility { get; set; }
        public List<ScheduleDTO> Schedules { get; set; }
        public List<PremisesDTO> SerieList { get; set; }
        public List<int> SchedulesIds { get; set; }
        public List<int> GroupsIds { get; set; }
        public List<GroupDTO> Groups { get; set; }
        public List<int?> UsersIds { get; set; }
        public List<UserDTO> Users { get; set; }
        public List<AbsenceDTO> AbsentUsers { get; set; }
        public List<InjuryUserDTO> InjuryUsers { get; set; }
        public List<HomeworkDTO> Homework { get; set; }
        public DateTime Timestamp { get; set; }
        public DateTime MadeTime { get; set; }
        public int? MadeBy { get; set; }
        public DateTime StartDateTime
        {
            get
            {
                if (Premises == null || Premises.Count == 0)
                {
                    return _StartDateTime;
                }
                return Premises.Min(t => t.TimeStart);
            }
            set
            {
                _StartDateTime = value;
            }
        }

        public DateTime EndDateTime
        {
            get
            {
                if (Premises == null || Premises.Count == 0)
                {
                    return _EndDateTime;
                }
                return Premises.Max(t => t.TimeEnd);
            }
            set
            {
                _EndDateTime = value;
            }
        }
        
        public UserDTO UpdatedByUser { get; set; }
        public string GroupsNamesString { get; set; }
        public string HomeWorkNamsString { get; set; }
        public string PremisesNamsString { get; set; }
        
    }
    public class CalendarAddEditVisibility
    {
        public bool Infoscreens { get; set; }
        public bool Website { get; set; }
    }
    public class CalendarAddEditSerie
    {
        public string Repeatday { get; set; }
        public DateTime SerieStart { get; set; }
        public DateTime SerieEnd { get; set; }
        public List<NotAllowDoubleDTO> NotAllowDouble { get; set; }
    }
    public class NotAllowDoubleDTO
    {
        public DateTime Date { get; set; }
        public int premisesId { get; set; }
    }
    public class CalendarEditSerieDTO
    {
        public int? Id { get; set; }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
        public DateTime BeforeStart { get; set; }
        public DateTime BeforeEnd { get; set; }
    }

    
    public class CalendarBlockDTO
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public int Minutes { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string Name { get; set; }
        public string PremisesName { get; set; }
        public string CreatorName { get; set; }
        public int DetailId { get; set; }
        public int MadeBy { get; set; }
        public DateTime MadeDate { get; set; }
        public bool MadeInViggo { get; set; }
        public int AttendeesCount { get; set; }
        public DateTime? CreatedTime { get; set; }
        public bool Private { get; set; }
        public bool ShowOnInfoscreen { get; set; }
        public bool ShowOnHomepage { get; set; }
        public bool Special { get; set; }
        public List<string> Premises { get; set; }
        public List<string> GroupNames { get; set; }
        public List<string> RelatedScheduleNames { get; set; }
        public List<HomeworkDTO> Homework { get; set; }
    }
    public class CalendarToViewTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public int ShowOnHomepage { get; set; }
        public int ShowOnInfoscreen { get; set; }
        public int Special { get; set; }
        public int Private { get; set; }
	    public int? MadeBy { get; set; }
        public DateTime MadeDate { get; set; }
        public int MadeInViggo { get; set; }
	    public int? PremisesId { get; set; }
        public string Premisesname { get; set; }
        public int RefCalendarPremisesId { get; set; }
	    public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }
        public DateTime Timestamp { get; set; }
        public string Place { get; set; }
	}
    public class CalendarCopy
    {
        public int OldId { get; set; }
        public int NewId { get; set; }
        public bool Available { get; set; }
    }

    public class CalendarInfoDTO
    {
        public List<int> scheduleList { get; set; }
        public bool special { get; set; }
        public bool noscheules { get; set; }
    }
    public class CalendarViggoDTO
    {
        public List<int> scheduleList { get; set; }
        public bool special { get; set; }
        public bool noscheules { get; set; }
        public bool privat { get; set; }
        public bool ownschedule { get; set; }
        public bool nogroupsorusers { get; set; }
        public bool guardOtherSchedules { get; set; }
        public int userId { get; set; }
        

    }
}
