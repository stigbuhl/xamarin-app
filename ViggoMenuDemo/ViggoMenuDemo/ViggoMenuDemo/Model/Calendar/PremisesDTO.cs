﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Guard;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Calendar
{
    public class PremisesDTO
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }
        public CalendarBlockDTO Calendar { get; set; }
        public List<CalendarBlockDTO> Calendars { get; set; }
        public GuardTypeDTO Guard { get; set; }
        public bool IsGuard { get; set; }
        public bool IsCalendars { get; set; }
        public bool IsAssigment { get; set; }
        public UserDTO UpdatetUser { get; set; }
        public DateTime Timestamp { get; set; }
        public int RefCalendarPremisesId { get; set; }
        public string borderColor { get; set; }
        public string backgroundColor { get; set; }
        public string color { get; set; }
    }
    public class AddEditPremisesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Folderid { get; set; }
    }
}
