﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;
using ViggoMenuDemo.Model.User.Post;

namespace ViggoMenuDemo.Model.Diary
{
    public class DiaryDTO
    {
        public int Id { get; set; }
        public int Updated_by { get; set; }
        public UserDTO UpdatedbyUser { get; set; }
        public DateTime? Timestamp { get; set; }
        public string Headline { get; set; }
        public string Contents { get; set; }
        public DateTime Date { get; set; }
        public List<UserDTO> Users { get; set; }
        public List<int> UsersIds { get; set; }
        public List<int> CategoryIds { get; set; }
        public List<DiaryCategoryDTO> Category { get; set; }
        public List<PostAnswerDTO> Answer { get; set; }
    }

    public class dbDiaryRelations
    {
        public int UserId { get; set; }
        public int DiaryId { get; set; }
        public string DiaryContents { get; set; }
        public DateTime DiaryDate { get; set; }
        public DateTime DiaryTimestamp { get; set; }
        public string DiaryHeadline { get; set; }
        public int UpdatedbyUserId { get; set; }
        public string UpdatedbyUserFirstname { get; set; }
        public string UpdatedbyUserMiddlename { get; set; }
        public string UpdatedbyUserLastname { get; set; }
        public string UpdatedbyUserImage { get; set; }
        public int? AnswerId { get; set; }
        public string AnswerContent { get; set; }
        public DateTime? AnswerTimestamp { get; set; }
        public int? AnswerUserId { get; set; }
        public string AnswerUserFirstname { get; set; }
        public string AnswerUserMiddlename { get; set; }
        public string AnswerUserLastname { get; set; }
        public string AnswerUserImage { get; set; }
        public int? DiaryCategoryId { get; set; }
        public string DiaryCategoryColor { get; set; }
        public string DiaryCategoryName { get; set; }
        public double? DiaryCategoryOrderby { get; set; }
    }

    public class DiaryCategoryDTO
    {
        public int Id { get; set; }
        public int RefDiaryId { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public double Orderby { get; set; }
    }
}
