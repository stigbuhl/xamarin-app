﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Guard
{
    public class GuardDTO
    {
        public int Id { get; set; }
        public UserDTO UpdatedBy { get; set; }
        public Nullable<DateTime> Timestamp { get; set; }
        public UserDTO User { get; set; }
        public DateTime Date { get; set; }
        public GuardTypeDTO Type { get; set; }
    }
    public class GuardTypeDTO
    {
        public int Id { get; set; }
        public UserDTO UpdatedBy { get; set; }
        public Nullable<DateTime> Timestamp { get; set; }
        public string Description { get; set; }
        public TimeSpan Timestart { get; set; }
        public TimeSpan Timeend { get; set; }

    }
}
