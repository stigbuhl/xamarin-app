﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Cms;

namespace ViggoMenuDemo.Model.Screen
{
    public class ScreenDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Key { get; set; }
        public string KeyString { get; set; }
        public string Theme { get; set; }
        public List<ScreenElementDTO> ScreenElements { get; set; }
        public List<ScreenElementDTO> ScreenElementsNoScreen { get; set; }
        public bool Selectet { get; set; }
        public int SelectetInt { get; set; }
        public int BorderSize { get; set; }
        public ImagesCmsDTO BgImage { get; set; }
        public int? BgImageId { get; set; }
        public string Language { get; set; }
    }
    public class ScreenElementDTO
    {
        //public int Id { get; set; }
        //public int[] Ids { get; set; }
        //public int X { get; set; }
        //public int Y { get; set; }
        //public int Width { get; set; }
        //public int Height { get; set; }
        //public int Index { get; set; }
        //public string Name { get; set; }
        //public ViggoScreenFunction ViggoScreenFunction { get; set; }
        //public int ScreenId { get; set; }
        //public string Class { get; set; }
        //public int ElementHeadlineSize { get; set; }
        //public int HeadlineSize { get; set; }
        //public int ContentSize { get; set; }
        //public string Effect { get; set; }
        //public int Effecttime { get; set; }
        //public List<ElementSettingsDTO> ElementSettings { get; set; }
        //public bool AutoScroll { get; set; }
    }
    public class ElementSettingsDTO
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
