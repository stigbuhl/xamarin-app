﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Filesharing
{
    public class FileDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public string Description { get; set; }
        public int FunctionId { get; set; }
        public int UserType { get; set; }
        public int FolderId { get; set; }
        public int ParentId { get; set; }
        public int Size { get; set; }
        public DateTime Timestamp { get; set; }

        //Filename (from filecode) is only relevant for services/model, not for presentation layer
        internal string FileName { get; set; }

        //Relations
        public int MadeById { get; set; }
        public UserDTO UpdatedByUser { get; set; }
        public FolderDTO ParentFolder { get; set; }
    }
}
