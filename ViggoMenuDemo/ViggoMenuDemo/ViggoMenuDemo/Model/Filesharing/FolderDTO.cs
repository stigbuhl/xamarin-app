﻿using System.Collections.Generic;
using ViggoMenuDemo.Model.Calendar;
using ViggoMenuDemo.Model.SchedulePlanning;
using ViggoMenuDemo.Model.SchedulePlanning.Elements;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Filesharing
{
    public class FolderDTO
    {
        //public int Id { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public int FunctionId { get; set; }
        //public int UserType { get; set; }
        //public int ParentFolderId { get; set; }
        //public string ParentFolderName { get; set; }
        //public int? MadeById { get; set; }
        //public int UpdatedById { get; set; }
        //public string FolderPath = string.Empty;
        //public UserDTO UpdatedByUser { get; set; }
        //public FolderDTO ParentFolder { get; set; }
        //public List<FileDTO> Files { get; set; }
        //public List<GroupDTO> Groups { get; set; }
        //public List<EvaluationDTO> Evaluations { get; set; }
        //public List<FormularDTO> Formulars { get; set; }
        //public List<PremisesDTO> Premises { get; set; }
        //public List<StructuresDTO> StructursItems { get; set; }
        //public List<CheckInOutDTO> CheckInOut { get; set; }
        //public List<PeriodDTO> Periods { get; set; }
        //public List<ScheduleElementDTO> Activity { get; set; }
        //public int? SchoolYear { get; set; }
        //public List<FolderAndContentsDTO> FolderAndContents { get; set; }
        //public List<FolderDTO> ChildFolders { get; set; }
        //public int CurrentFolderDiskSpace { get; set; }
        //public bool ShowDiskSpace { get; set; }
        //public long FolderDiskSpace { get; set; }
        //public bool ShowFolderRights { get; set; }
        //public FolderFilSharingAuthorizationDTO FolderFilSharingAccess { get; set; }
        //public int Hidden { get; set; }
        //public bool PublicScreen { get; set; }
        //public bool AnyAccess { get; set; }
        //public int Orderby { get; set; }
        //public List<int> CurentuserGroups { get; set; }
    }
    public class FolderSimpelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int FunctionId { get; set; }
        public int UserTypeId { get; set; }
        public int ParentFolderId { get; set; }
        public long? FolderDiskSpace { get; set; }
        public string AuthorizationLevelRead { get; set; }
        public string AuthorizationLevelUpload { get; set; }
        public string AuthorizationLevelCreateFolder { get; set; }
        public string AuthorizationLevelDeleteUpdate { get; set; }
        public string AuthorizationLevelGrantPermission { get; set; }
        public int? Hidden { get; set; }
        public bool AnyAccess { get; set; }
        public int? MadeById { get; set; }
        public bool PublicScreen { get; set; }
    }
    public class FolderSizeDbDTO
    {
        public int Id { get; set; }
        public int FunctionId { get; set; }
        public int? UserType { get; set; }
        public int ParentFolderId { get; set; }
        public long FolderDiskSpace { get; set; }
        public string FolderDiskSpaceString { get; set; }
        public bool UserDeleted { get; set; }
        public int UserId { get; set; }
        public string UserFullName { get; set; }
    }
    public class FolderFilSharingAuthorizationDTO
    {
        public List<int> ReadGroups { get; set; }
        public List<int> UploadGroups { get; set; }
        public List<int> CreateFolderGroups { get; set; }
        public List<int> DeleteUpdateGroups { get; set; }
        public List<int> GrantPermissionGroups { get; set; }

        public List<GroupDTO> ReadGroupsDto { get; set; }
        public List<GroupDTO> UploadGroupsDto { get; set; }
        public List<GroupDTO> CreateFolderGroupsDto { get; set; }
        public List<GroupDTO> DeleteUpdateGroupsDto { get; set; }
        public List<GroupDTO> GrantPermissionGroupsDto { get; set; }
        public bool UpdateAllSubfolders { get; set; }
    }
    public class RoomFolderDTO
    {
        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }
        public FolderDTO Folder { get; set; }
    }
    //public class FolderAndContentsDTO
    //{
    //    public List<FolderDTO> Folders { get; set; }
    //    public List<ScheduleSetupElementDetailsDTO> ElementSetupDetails { get; set; }
    //    public int FolderId { get; set; }

    //}
}