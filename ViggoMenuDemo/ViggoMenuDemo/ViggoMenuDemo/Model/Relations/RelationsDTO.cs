﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Relations
{
    public class RelationsDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public DateTime? TimeStamp { get; set; }
        // public List<RelationsUsersDTO> UsersRelations { get; set; } 
    }
    public class RelationsMasterUserDTO
    {
        public int Id { get; set; }
        public int MasterUserId { get; set; }
        public UserDTO MasterUser { get; set; }
        public int CountChildUsers { get; set; }
        public int RelationsId { get; set; }
        public string RelationsName { get; set; }
        //public string Ref_users_Id { get; set; }
        public string ChildUsersToIds { get; set; }
    }
}
