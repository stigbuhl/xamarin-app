﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Cms;
using ViggoMenuDemo.Model.Filesharing;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Menu.Formular
{
    public class FormularDTO
    {
        public int Id { get; set; }
        public string Headline { get; set; }
        public string Contents { get; set; }
        public int Editafterwards { get; set; }
        public int FolderId { get; set; }
        public DateTime Timestart { get; set; }
        public DateTime Timeend { get; set; }
        public bool OnlyOneAnswer { get; set; }
        public FormularGroupsDTO UserGroupsAttached { get; set; }
        public string AcesseGroupsReadIds { get; set; }
        public string AcesseGroupsEditIds { get; set; }
        public string AcesseGroupsSettingsIds { get; set; }
        public string AnswerGroupsIds { get; set; }
        public string AnswerGroupsRelationsIds { get; set; }
        public string AnswerRelationsIds { get; set; }
        public List<UsersFormularDTO> Answerusers { get; set; } //(dem som har svaret)
        public List<UsersFormularDTO> Addressedusers { get; set; } //(Dem som ikke har svaret) - Gruppenavne,  navn, image, id
        public DateTime? Timestamp { get; set; }
        public int UpdatedBy { get; set; }
        public List<ElementsDTO> Elements { get; set; }
        public List<FolderDTO> Folder { get; set; }
        public bool Copy { get; set; }
        public int CopyId { get; set; }
        public string Button { get; set; }
        public UserDTO UpdatedByUser { get; set; }
        public List<PageDTO> Pages { get; set; }
        public string AcceptedHeadline { get; set; }
        public string AcceptedContents { get; set; }
        public string WaitHeadline { get; set; }
        public string WaitContents { get; set; }
        public string ClosedHeadline { get; set; }
        public string ClosedContents { get; set; }
        public int? MaxAnswers { get; set; }
        public string ContactMail { get; set; }
        public int MadeInModul { get; set; }
        public bool Uniq { get; set; }
        public int? MadeById { get; set; }
    }
    public class ElementsDTO
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Description { get; set; }
        public int? Width { get; set; }
        public int? Orderby { get; set; }
        public bool Needed { get; set; }
        public string Value { get; set; }
        public bool Locked { get; set; }
        public List<ElementsDTO> SubElements { get; set; }
        public DateTime? Timestamp { get; set; }
        public int UpdatedBy { get; set; }
        public int FormularId { get; set; }
        public int LimitAmount { get; set; }
        public bool VisibleForOtherUsers { get; set; }
        public bool Disabled { get; set; }
        public int? AmountOfAnswers { get; set; }
        public FormularAnswerDTO Answer { get; set; }
        public int? CrmElementId { get; set; }
        //public List<> UsersHoHaveAnswers { get; set; }
    }

    public class dbElements
    {
        public int ElementId { get; set; }
        public string Headline { get; set; }
        public string ElementDescription { get; set; }
        public bool ElementLocked { get; set; }
        public bool ElementNeeded { get; set; }
        public int? ElementOrderBy { get; set; }
        public int ElementType { get; set; }
        public string ElementValue { get; set; }
        public int? ElementWidth { get; set; }
        public int? ElementLimitamount { get; set; }
        public bool ElementVisibleForOtherUsers { get; set; }
        public int? ElementSubId { get; set; }
        public string ElementSubDescription { get; set; }
        public string ElementSubValue { get; set; }
        public int? ElementSubLimitamount { get; set; }
    }
    public class dbElementsAnswer
    {
        public int UserId { get; set; }
        public int? SubUserId { get; set; }

        public int FormularEditafterwards { get; set; }
        public string FormularHeadline { get; set; }
        public string FormularContents { get; set; }
        public bool FormularUnicq { get; set; }
        public string FormularButton { get; set; }

        public int ElementId { get; set; }
        public string ElementDescription { get; set; }
        public bool ElementLocked { get; set; }
        public bool ElementNeeded { get; set; }
        public int? ElementOrderBy { get; set; }
        public int ElementType { get; set; }
        public string ElementValue { get; set; }
        public int? ElementWidth { get; set; }
        public int? ElementLimitamount { get; set; }
        public bool ElementVisibleForOtherUsers { get; set; }

        public int? ElementSubId { get; set; }
        public string ElementSubDescription { get; set; }
        public string ElementSubValue { get; set; }
        public int? ElementSubLimitamount { get; set; }

        public int? AnswerId { get; set; }
        public DateTime? AnswerTimestamp { get; set; }
        public int? AnswerUserId { get; set; }
        public string AnswerValue { get; set; }
        public int? AnswerElementsId { get; set; }
    }
    public class FormularElementsAnswerDTO
    {

        public int Id { get; set; }
        public int Type { get; set; }
        public string Description { get; set; }
        public int? Width { get; set; }
        public int? Orderby { get; set; }
        public bool Needed { get; set; }
        public string Value { get; set; }
        public bool Locked { get; set; }
        public List<ElementsDTO> SubElements { get; set; }
        public DateTime? Timestamp { get; set; }
        public int UpdatedBy { get; set; }
        public int FormularId { get; set; }
        public int LimitAmount { get; set; }
        public bool VisibleForOtherUsers { get; set; }
        public bool Disabled { get; set; }
        public int? AmountOfAnswers { get; set; }
        //public List<> UsersHoHaveAnswers { get; set; }
    }
    public class FormularUserViewDTO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int? SubUserId { get; set; }
        public string Headline { get; set; }
        public DateTime AnswerTimestamp { get; set; }
    }
    public class FormularAnswerDTO
    {
        public int Id { get; set; }
        public int ElementId { get; set; }
        public string Answer { get; set; }
        public int AnswerUserId { get; set; }
        public DateTime TimeStamp { get; set; }
        public ElementsDTO Elements { get; set; }
        public int? AnswerForUserId { get; set; }
    }
    public class FormularAnswerForUserDTO
    {
        public FormularDTO Formular { get; set; }
        public List<ElementsDTO> Elements { get; set; }
        public int UserId { get; set; }
        public int? AnswerForUserId { get; set; }
        public UserDTO AnswerForUser { get; set; }
    }
    public class FormularUserAnswerDTO
    {
        public int FormularId { get; set; }
        public string Button { get; set; }
        public UserDTO User { get; set; }
        public int UserId { get; set; }
        public DateTime? AnswerTimestamp { get; set; }
        public int? AnswerUserId { get; set; }
        public UserDTO AnswerUser { get; set; }
        public List<FormularAnswerDTO> FormularAnswers { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool Create { get; set; }
        public string AcesseGroupsIds { get; set; }
        public FormularDTO FormularDto { get; set; }
        public int? AnswerForUserId { get; set; }
        public UserDTO AnswerForUser { get; set; }
        public int RefFormularCmsId { get; set; }
        public bool Uniq { get; set; }
        public bool Waitlist { get; set; }
    }
    public class FormularAnswerStatusDTO
    {
        //public int ref_group_formularId { get; set; }
        public int FormularId { get; set; }
        public bool OnlyOneAnswer { get; set; }
        public List<FormularUserDTO> AllUsers { get; set; }
        public List<FormularUsersDTO> Answer { get; set; }
        public List<FormularUsersDTO> NotAnswer { get; set; }
    }
    public class FormularCountsDTO
    {
        public int NumberOfAnswers { get; set; }
        public int NumberOfAddressed { get; set; }
        public int NumberOfRelations { get; set; }
        public int NumberOfAnswersRelations { get; set; }
        public int NumberOfElements { get; set; }

    }
    public class CmsFormularAnswerDTO
    {
        public int FormularId { get; set; }
        public int AnswerId { get; set; }
        public List<FormularAnswerDTO> FormularAnswers { get; set; }
        public DateTime TimeStamp { get; set; }
        public List<ElementsDTO> Element { get; set; }
        public string Button { get; set; }
        public bool Waitlist { get; set; }
    }
}
