﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Relations;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Menu.Formular
{
    public class FormularGroupsDTO
    {
        public FormularGroupDTO AcesseGroupsReadIds { get; set; }
        public FormularGroupDTO AcesseGroupsEditIds { get; set; }
        public FormularGroupDTO AcesseGroupsSettingsIds { get; set; }
        public List<FormularGroupAnswerDTO> FormularUsers { get; set; }

    }

    public class FormularGroupDTO
    {

        public List<GroupDTO> Groups { get; set; }
        public int UserCount { get; set; }

    }



    public class FormularGroupAnswerIdsDTO
    {
        public int FormularId { get; set; }
        public bool Unique { get; set; }
        public List<int> GroupsIds { get; set; }
        public List<int> RelationsIds { get; set; }
    }
    public class FormularGroupAnswerDTO
    {
        public int FormularId { get; set; }
        public bool Unique { get; set; }
        public GroupDTO Group { get; set; }
        public List<FormularUsersDTO> Users { get; set; }
    }
    public class FormularUsersDTO
    {
        public UserDTO User { get; set; }
        public RelationsDTO Relations { get; set; }
        public bool Answer { get; set; }
        public int UsersSubId { get; set; }
        public List<FormularUsersDTO> UsersSub { get; set; }
        public List<FormularAnswerDTO> FormularAnswers { get; set; }
        public DateTime FormularAnswersTime { get; set; }
    }

    public class dbFormularUsers
    {
        public int ElevId { get; set; }
        public string ElevFirstname { get; set; }
        public string ElevMiddlename { get; set; }
        public string ElevLastname { get; set; }
        public string ElevImage { get; set; }
        public int? MasterUserId { get; set; }
        public string MasterUserFirstname { get; set; }
        public string MasterUserMiddlename { get; set; }
        public string MasterUserLastname { get; set; }
        public string MasterUserImage { get; set; }
        public int? RelationsId { get; set; }
        public string RelationsName { get; set; }
    }

    public class dbFormularAnswers
    {
        public int ElementsId { get; set; }
        public int AnswerForUserId { get; set; }
        public int AnswerId { get; set; }
        public string Answer { get; set; }
        public int UserId { get; set; }
        public DateTime AnswersTimestamp { get; set; }
        public int AnswerUserId { get; set; }
        public string AnswerFirstname { get; set; }
        public string AnswerMiddlename { get; set; }
        public string AnswerLastname { get; set; }
        public string AnswerImage { get; set; }
    }

    public class FormularUserDTO
    {
        private DateTime _AnswersTimestamp { get; set; }
        private UserDTO _AnswersBy { get; set; }
        public bool NeedToAnswer { get; set; }
        public int UserId { get; set; }
        public UserDTO User { get; set; }
        public List<FormularUserRelationsDTO> Relations { get; set; }
        public List<FormularAnswerDTO> FormularAnswers { get; set; }
        public UserDTO AnswerBy
        {
            get
            {
                if (FormularAnswers == null || FormularAnswers.Count == 0)
                {
                    return _AnswersBy;
                }
                var user = FormularAnswers.OrderByDescending(t => t.TimeStamp).First();
                UserDTO u = null;

                if (User.Id == user.AnswerUserId)
                {
                    u = User;
                }
                else
                {
                    u = Relations.Select(t => t.User).FirstOrDefault(t => t.Id == user.AnswerUserId);
                    if (u == null)
                    {
                        u = _AnswersBy;
                    }
                }
                return u;
            }
            set { _AnswersBy = value; }
        }
        public DateTime AnswersTimestamp
        {
            get
            {
                if (FormularAnswers == null || FormularAnswers.Count == 0)
                {
                    return _AnswersTimestamp;
                }
                return FormularAnswers.Max(t => t.TimeStamp);
            }
            set { _AnswersTimestamp = value; }

        }


    }
    public class FormularUserRelationsDTO
    {
        public bool NeedToAnswer { get; set; }
        public UserDTO User { get; set; }
        public string RelationsName { get; set; }
    }
}
