﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Menu.Formular;
using ViggoMenuDemo.Model.Absence;
using ViggoMenuDemo.Model.Bulletin;
using ViggoMenuDemo.Model.Calendar;
using ViggoMenuDemo.Model.Diary;
using ViggoMenuDemo.Model.Evaluation;
using ViggoMenuDemo.Model.Message;
using ViggoMenuDemo.Model.Room;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Home
{
    public class HomeDTO
    {
        public int Id { get; set; }
        public bool Visible { get; set; }
        public double OrderBy { get; set; }
        public int Side { get; set; } //1=Left 2=Right
        public int Functionid { get; set; }
        public int? UserType { get; set; }
        public string UserTypeName { get; set; }
        public int Show { get; set; }
        public DateTime LastUpdatet { get; set; }
        public List<MessageDTO> MessageDto { get; set; }
        public List<DiaryDTO> DiaryDto { get; set; }
        public List<BulletinDTO> BulletinDto { get; set; }
        public List<PremisesDTO> ScheduleDto { get; set; }
        public List<AbsenceDTO> AbsenceDto { get; set; }
        // public List<FormularUserAnswerDTO> FormularDto { get; set; }
        public List<RoomHomeDTO> RoomDto { get; set; }
        public RelationsMasterChildUsers RelationsDto { get; set; }
    }

    public class HomeAbsenceDTO
    {
        public List<int> ExcludeCauses { get; set; }
        public List<AbsenceCauseDTO> AllCauses { get; set; }
        public List<int> ExcludeUsertyps { get; set; }
        public bool IsMore { get; set; }
        public List<AbsenceDTO> AbsenceList { get; set; }
        public int Show { get; set; }
    }

    public class dbHomeRelations
    {
        public int UserId { get; set; }
        public string UserFirstname { get; set; }
        public string UserMiddlename { get; set; }
        public string UserLastname { get; set; }
        public string UserImage { get; set; }
    }

    public class RelationsMasterChildUsers
    {
        public List<UserDTO> User { get; set; }
        public UserDTO MasterUser { get; set; }
    }

    public class HomeRelationsDTO
    {
        public UserDTO User { get; set; }
        public List<AbsenceDTO> Absence { get; set; }
        public List<DiaryDTO> Diary { get; set; }
        public List<EvaluationEditAnswerViewDTO> Evaluation { get; set; }
        public List<CalendarDTO> Calendar { get; set; }
        public List<FormularUserViewDTO> Formular { get; set; }

        //public HomeRelationsDTO(JArray array)
        //{
        //    User = array[0]["User"].ToObject<UserDTO>();
        //    if (array[0]["Absence"] != null || array["Absence"].HasValues)
        //    {
        //        Absence = array[0]["Absence"].ToObject<List<AbsenceDTO>>();
        //    }
        //    if (array[0]["Diary"] != null || array[0]["Diary"].HasValues)
        //    {
        //        Diary = array[0]["Diary"].ToObject<List<DiaryDTO>>();
        //    }            
        //}
    }
}
