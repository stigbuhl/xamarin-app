﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Cms
{
    public class SettingDTO
    {
        public int Id { get; set; }
        public int NrOf_News { get; set; }
        public int NrOf_Smallnews { get; set; }
        public int NewsOrder { get; set; }
        public int SmallNewsOrder { get; set; }
        public sbyte? TopbarAction { get; set; }
        public string IncludeCode { get; set; }
        public DateTime Timestamp { get; set; }
        public UserDTO UpdatedbyUser { get; set; }
        public string Metatags { get; set; }
        public string MetaDescription { get; set; }
        public string BrowserName { get; set; }
    }
}
