﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Cms
{
    public class CollectionDTO
    {
        //public int Id { get; set; }
        //public string Name { get; set; }
        //public string ImgesUrl { get; set; }
        //public string Description { get; set; }
        //public List<ImagesCmsDTO> Images { get; set; }
        //public List<int> ImagesIds { get; set; }
        //public UserDTO UpdatedbyUser { get; set; }
        //public DateTime Timestamp { get; set; }
        //public ViggoCmsImageType Type { get; set; }
        //public bool Standard { get; set; }
        //public int RefPagesCollectionId { get; set; }
    }
    public class ImagesCmsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImgesUrl { get; set; }
        public string ImgesBigUrl { get; set; }
        public string Description { get; set; }
        public UserDTO UpdatedbyUser { get; set; }
        public DateTime Timestamp { get; set; }
        public ViggoCmsImageType Type { get; set; }
        public bool Standard { get; set; }
        public int RefCollectionId { get; set; }
    }

    public class ViggoCmsImageType
    {
    }

    public class TopbarDTO
    {
        public List<ImagesCmsDTO> Images { get; set; }
        public List<CollectionDTO> Collection { get; set; }
    }
}
