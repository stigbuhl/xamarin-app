﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Cms
{
    public class ElementDTO
    {
        public int Id { get; set; }
        public int RefElementPage { get; set; }
        public int PlacementId { get; set; }
        public UserDTO UpdatedbyUser { get; set; }
        public DateTime Timestamp { get; set; }
        public string Description { get; set; }
        public string Elementname { get; set; }
    }
    public class PlacmentDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
