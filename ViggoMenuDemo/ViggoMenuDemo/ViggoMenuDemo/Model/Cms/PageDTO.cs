﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Menu.Formular;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Cms
{
    public class PageDTO
    {
        public int Id { get; set; }
        public string Linkname { get; set; }
        public string Headline { get; set; }
        public string Contents { get; set; }
        public int ChildId { get; set; }
        public List<PageDTO> ChildPages { get; set; }
        public UserDTO UpdatedbyUser { get; set; }
        public DateTime Timestamp { get; set; }
        public string Title { get; set; }
        public bool Visible { get; set; }
        public bool ShowDate { get; set; }
        public bool ShowUpdatedBy { get; set; }
        public bool ShowGalleryThumbs { get; set; }
        public int? OrderBy { get; set; }
        public string Url { get; set; }
        public bool CollectionTopbar { get; set; }
        public string TopBar { get; set; }
        public string TopBarUrl { get; set; }
        public string MetaData { get; set; }
        public FormularUserAnswerDTO FormularUserAnswer { get; set; }
        public FormularDTO Formular { get; set; }
        public string GalleryThumb { get; set; }

        public CollectionDTO Collection { get; set; }
        public List<ElementDTO> Element { get; set; }
        public SettingDTO MetadataSettings { get; set; }
    }
    public class PageMenuDTO
    {
        public int Id { get; set; }
        public string Linkname { get; set; }
        public string Url { get; set; }
        public bool Selected { get; set; }
        public List<PageMenuDTO> ChildMenu { get; set; }
        public int Parentid { get; set; }
        public int? OrderBy { get; set; }

    }
    public class PageBreadCrumbsDTO
    {
        public int Id { get; set; }
        public string Linkname { get; set; }
        public string Url { get; set; }
        public bool Selected { get; set; }
        public int OrderBy { get; set; }
    }
}
