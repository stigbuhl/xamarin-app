﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViggoMenuDemo.Model.User
{
    public class UsersFormularDTO
    {
        public UserDTO User { get; set; }
        public string GroupNames { get; set; }
        public bool Answer { get; set; }
        public int ReferenceId { get; set; }
        public int? AnswerId { get; set; }
        public UserDTO LockedBy { get; set; }
        public DateTime? Lockeddate { get; set; }
    }
    public class UsersFormularDbDTO
    {
        public int UserId { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string GroupNames { get; set; }
        public string Image { get; set; }
        public bool Answer { get; set; }
        public int? AnswerId { get; set; }
        public int ReferenceId { get; set; }
        public DateTime? Lockeddate { get; set; }
        public int? LockedbyId { get; set; }

    }
}
