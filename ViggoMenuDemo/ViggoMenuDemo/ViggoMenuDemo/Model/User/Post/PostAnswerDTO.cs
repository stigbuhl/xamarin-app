﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Filesharing;

namespace ViggoMenuDemo.Model.User.Post
{
    public class PostwAnswersDTO
    {
        public PostDTO Post { get; set; }
        public List<PostAnswerDTO> Answers { get; set; }
        public string OrderAnswers { get; set; }
    }
    /// <summary>
    /// Gets the answers for an actual post
    /// Properties: Content, Updated_by, Timestamp
    /// </summary>
    public class PostAnswerDTO
    {
        //Used for test reason
        public int Id { get; set; }
        public int PostId { get; set; }
        //The content of the answer
        public string Content { get; set; }
        //id of the user who wrote the answer
        public UserDTO UpdatedBy { get; set; }
        //The time the answer was sent
        public DateTime? Timestamp { get; set; }
        public List<FileDTO> AttachmentsFiles { get; set; }
    }
}
