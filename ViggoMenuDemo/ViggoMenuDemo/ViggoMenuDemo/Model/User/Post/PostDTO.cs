﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Filesharing;

namespace ViggoMenuDemo.Model.User.Post
{
    public class PostDTO
    {
        public int Id { get; set; }
        public string Icon { get; set; }
        public Nullable<DateTime> TimeStart { get; set; }
        public Nullable<DateTime> TimeEnd { get; set; }
        public int Updated_by { get; set; }
        public UserDTO UpdatedbyUser { get; set; }
        public Nullable<DateTime> Timestamp { get; set; }
        public string Headline { get; set; }
        public string Contents { get; set; }
        public int AnswerCount { get; set; }
        public bool AccessToWrite { get; set; }
        public bool Readed { get; set; }
        public List<FileDTO> AttachmentsFiles { get; set; }
    }
}
