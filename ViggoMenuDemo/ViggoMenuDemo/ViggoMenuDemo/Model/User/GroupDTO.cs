﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Filesharing;

namespace ViggoMenuDemo.Model.User
{
    public class GroupDTO
    {
        public GroupDTO()
        {
            Remove = false;
        }
        // public List<GroupDTO> Groups { get; set; }
        public List<UserDTO> Users { get; set; }
        public List<UserDTO> Teachers { get; set; }
        public List<int> UsersIds { get; set; }
        public string UsersIdsString { get; set; }
        public List<int> ContractTeacherIds { get; set; }
        public string ContractTeacherString { get; set; }
        public bool ContractTeacher { get; set; }
        public string Name { get; set; }
        public string OriginalName { get; set; }
        public FolderDTO Categories { get; set; }
        public int FolderId { get; set; }
        public string FolderName { get; set; }
        public bool Visible { get; set; }
        public int CountMemberInGroup { get; set; }
        public string Description { get; set; }
        public int UpdatedBy { get; set; }
        public UserDTO UpdatedByUser { get; set; }
        public DateTime TimeStamp { get; set; }
        public int Id { get; set; }
        public bool Selected { get; set; }
        public string SyncId { get; set; }
        public bool Remove { get; set; }
        public string SearchText
        {
            get
            {
                return (Name).ToLower();
            }
        }
        //public ViggoGroupType GroupType { get; set; }
        public string NameFirstPriority { get; set; }

    }
    public class SearchGroup
    {
        public List<GroupDTO> Groups { get; set; }
        public List<FolderDTO> Folders { get; set; }

    }
}
