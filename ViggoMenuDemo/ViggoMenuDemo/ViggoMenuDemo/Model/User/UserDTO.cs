﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViggoMenuDemo.Model.User
{
    /// <summary>
    /// Data Transfer Object
    /// </summary>
    public class UserDTO
    {
        //Quick-get for fullname
        public string FullName
        {
            get
            {
                var s = "";
                if (!string.IsNullOrEmpty(Firstname))
                    s = Firstname.Trim();
                if (!string.IsNullOrEmpty(Middlename))
                    s += " " + Middlename.Trim();
                if (!string.IsNullOrEmpty(Lastname))
                    s += " " + Lastname.Trim();
                return s;
            }
        }

        /// <summary>
        /// No need to define this id since it is made in the database
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// reference key for title
        /// </summary>
        public string Initials { get; set; }

        /// <summary>
        /// Firstname
        /// </summary>
        public string Firstname { get; set; }

        /// <summary>
        /// Surname
        /// </summary>
        public string Middlename { get; set; }

        /// <summary>
        /// Surname
        /// </summary>
        public string Lastname { get; set; }

        /// <summary>
        /// Social Security number
        /// </summary>
        public string Cpr { get; set; }

        /// <summary>
        /// Sex
        /// </summary>
        public int? Sex { get; set; }
        public string Grade { get; set; }
        /// <summary>
        /// Roadname and number
        /// </summary>
        public string Address { get; set; }
        public bool ProtectedAddress { get; set; }
        /// <summary>
        /// City name
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// City name
        /// </summary>
        public string LocalCity { get; set; }

        /// <summary>
        /// Postcode
        /// </summary>
        public string Zipcode { get; set; }

        /// <summary>
        /// Primary phonenumber (landline)
        /// </summary>
        public string Phone { get; set; }
        public bool ProtectedPhone { get; set; }
        /// <summary>
        /// Primary cellphone number
        /// </summary>
        public string Mobile { get; set; }
        public bool ProtectedMobile { get; set; }
        /// <summary>
        /// Primary cellphone number
        /// </summary>
        public string WorkPhone { get; set; }
        public bool ProtectedWorkPhone { get; set; }
        /// <summary>
        /// Phonenumber of primary contact
        /// </summary>
        public string PhoneContact { get; set; }
        public bool ProtectedPhoneContact { get; set; }
        /// <summary>
        /// Email adress (primary username)
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Users birthday
        /// </summary>
        ///  [DataType(DataType.Date)]
        public DateTime? Birthday { get; set; }


        public bool NotAllowSync { get; set; }

        /// <summary>
        /// path to image of User
        /// </summary>
        public string ImageSmall { get; set; }

        /// <summary>
        /// path to image of User
        /// </summary>
        public string ImageBig { get; set; }
        /// <summary>
        /// path to image of User
        /// </summary>
        public string ImageRootBig { get; set; }

        /// <summary>
        /// path to webimage of user
        /// </summary>
        public string Webimage { get; set; }

        /// <summary>
        /// Description of user
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Description for CMS module (homepage)
        /// </summary>
        public string DescriptionHomepage { get; set; }

        /// <summary>
        /// in the datebase the users don't get deleted instead they get a sbyte status
        /// if 0 = not deleted user, 1 = deleted user
        /// </summary>
        public sbyte? Deleted { get; set; }

        /// <summary>
        /// Registering who edited this user's information last
        /// </summary>
        public int UpdatedById { get; set; }
        public UserDTO UpdatedByUser { get; set; }
        /// <summary>
        /// Studentid of user
        /// </summary>
        public int? StudentId { get; set; }
        /// <summary>
        /// If you are administrator or not administrator, 1 = administrator 0 = not administrator
        /// </summary>
        public bool Administrator { get; set; }

        /// <summary>
        /// Users homepage
        /// </summary>
        public string Homepage { get; set; }

        /// <summary>
        /// Some specific note about the user 
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Users country of origin
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// UNI-C username
        /// </summary>
        public string UniCUser { get; set; }

        /// 
        public string SyncId { get; set; }
        /// <summary>
        /// Timestamp for last edit of user in db
        /// </summary>
        public DateTime? Timestamp { get; set; }

        /// <summary>
        /// Timestamp for last edit of user in db
        /// </summary>
        public DateTime? ExternsyncTimestamp { get; set; }
        /// <summary>
        /// List of UserTypeDTO's relevant for this user
        /// </summary>
        public List<UserTypeDTO> UserTypes { get; set; }

        /// <summary>
        /// Defines the usertypes the user can be assigned with
        /// usertypeIds directly used by the Authorization process
        /// </summary>
        public List<int> UserTypeIds { get; set; }

        public List<UserProfileDTO> Profil { get; set; }
        public ModulaclDTO Modulacl { get; set; }
        public AbsenceDTO Absence { get; set; }
        public List<RelationsUsersDTO> Relations { get; set; }
        public List<SyncRelationsUsersDTO> SyncRelations { get; set; }
        public string SyncStudentRoom { get; set; }
        public string SyncStudentHouse { get; set; }
        public string Schoolline { get; set; }
    }

    public class UserSubstitute
    {
        public UserDTO User { get; set; }
        public string SubstituteNote { get; set; }
        public long? SubstituteForUserId { get; set; }
        public UserDTO SubstituteForUser { get; set; }
        public bool SubstituteVisibleScreen { get; set; }
    }

    public class SyncRelationsUsersDTO
    {
    }

    public class RelationsUsersDTO
    {
    }

    public class ModulaclDTO
    {
    }

    public class UserProfileDTO
    {
    }
}
