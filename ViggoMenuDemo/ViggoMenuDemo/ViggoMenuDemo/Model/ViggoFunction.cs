﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViggoMenuDemo.Model
{
    public enum ViggoFunction
    {

        Absence = 5, //Fravær
        AbsenceComments = 172,
        AbsenceCauses = 96,
        AbsenceInProcent = 31,
        AbsencePlace = 95,
        AccessRelationsAbsence = 16,
        AccessRelationsCharacter = 19,
        AccessRelationsCreateAbsence = 107,
        AccessRelationsDiary = 17, //usertyps dagbog + relation
        AccessRelationsEvaluation = 15,
        AccessRelationsDiaryComments = 131, //usertyps dagbog + relation
        AccessRelationsFiles = 204,
        AccessRelationsFormularAnswers = 105,
        AccessRelationsHomework = 18,
        AccessRelationsSchedule = 14,
        AccessRelationsSendMessageToGroupsRelations = 106,
        AccessRelationsInjurys = 170,
        ScheduleElement = 168,
        Administrator = 0,
        AdminUsers = -27,
        AllArrangements = 173,
        CheckInOut = 132,
        Answer = 59, //to find out which topics have been read. Table: posts_answer
        ApiSync = 92,
        Archive = 28,
        Assignment = 120,
        AssignmentComment = 121,
        AssignmentCommentMulti = 122,
        Bulletin = 20, //Opslag - SUB
        BulletinShowOnScreen = 21,
        BulletinShowToUsers = 22,
        Charactar = 25,
        CharactarPersonal = 26,
        ClientImg = 93,
        CmsCollection = 97,
        CmsFormular = 113,
        CmsGalleri = 100,
        CmsImages = 98,
        CmsNews = 99,
        CmsPages = 94,
        CmsTopbar = 101,
        Crm = 148,
        CrmAssignmentsSettings = 153,
        CrmStatus = 155,
        CrmSettings = 149,
        CrmSettingsImport = 63,
        CrmType = 154,
        CrmUpdates = 150,
        Diary = 32,
        DiaryAddComments = 127,
        DiaryCategory = 129,
        EdbBrugsTeams = 124,
        EmailClient = 33,
        Error = 119,
        Evaluation = 11,
        EvaluationComments = 171,
        ExaminationsCensor = 202,
        ExcludeFromMessage = 203,
        FileSharing = 8, // Fildeling - SUB
        ForgotPassword = 88,
        Formular = 30,
        GradeGroups = 144,
        Groups = 7, //Grupper
        Guard = 12,
        GuardType = 91,
        Guide = 145,
        Help = 125,
        Homework = 23,
        ImportUsers = 102,
        Injury = 165,
        InjurysOwnBasic = 156,
        InjurysOwnAdvanced = 157,
        InjurysOwnJournal = 158,
        InjurysBasic = 159,
        InjurysAdvanced = 160,
        InjurysJournal = 161,
        InjurysTherapistAcces = 162,
        Message = 1, //Beskeder
        MessageToRelations = 123, //Kan sende til brugeres relationer
        Notification = 65,
        OtherUsersSchedule = 13,
        OwnAbsence = 114,
        OwnAbsenceNote = 120,
        OwnDiary = 115,
        OwnEvaluations = 116,
        OwnFolder = 9,
        OwnGroups = 117,
        OwnRelations = 118,
        OwnWorkTime = 207,
        #region ScheduleAndTime
        ScheduleAndTimeSubject = 136,
        ScheduleAndTimeActivity = 137,
        ScheduleAndTimePreparation = 138,
        SchoolYears = 139,
        ScheduleAndTimePresence = 140,
        ScheduleAndTimeTask = 141,
        ScheduleAndTimeSupervision = 206,
        #endregion
        Premises = 4, //lokale
        Print = 10,
        #region Edit on user
        ProfileAdditional = 44,
        ProfileAddress = 38,
        ProfileBirthday = 43,
        ProfileCityAndZipcode = 39,
        ProfileContactPhone = 42,
        ProfileCountry = 53,
        ProfileCpr = 36,
        ProfileDescription = 49,
        ProfileEmail = 34,
        ProfileGrade = 142,
        ProfileHomepage = 50,
        ProfileHouse = 133,
        ProfileImage = 61,
        ProfileImageAdmin = 164,
        ProfileInitials = 111,
        ProfileMobile = 41,
        ProfileName = 37,
        ProfilePassword = 55,
        ProfilePhone = 40,
        //ProfilePictureWebsite = 51,
        //ProfileRelations = 64,
        ProfileRoom = 46,
        ProfileSex = 60,
        ProfileStudentId = 35,
        //ProfileTitle = 52,
        ProfileUniccUser = 54,
        ProfileWorkphone = 108,
        #endregion
        #region Edit on relations
        ProfileRelations = 62,
        //ProfileRelationsAbsence = 175,
        ProfileRelationsAdditional = 176,
        ProfileRelationsAddress = 177,
        ProfileRelationsBirthday = 178,
        ProfileRelationsCityAndZipcode = 179,
        ProfileRelationsContactPhone = 180,
        ProfileRelationsCountry = 181,
        ProfileRelationsCpr = 182,
        ProfileRelationsDescription = 183,
        ProfileRelationsEmail = 184,
        //ProfileRelationsGroups = 185,
        ProfileRelationsGrade = 186,
        ProfileRelationsHomepage = 187,
        ProfileRelationsHouse = 188,
        ProfileRelationsImage = 189,
        ProfileRelationsImageAdmin = 190,
        ProfileRelationsInitials = 191,
        ProfileRelationsMobile = 192,
        ProfileRelationsName = 193,
        ProfileRelationsPassword = 194,
        ProfileRelationsPhone = 195,
        ProfileRelationsRoom = 196,
        ProfileRelationsSex = 197,
        ProfileRelationsStudentId = 198,
        ProfileRelationsUniccUser = 199,
        ProfileRelationsWorkphone = 200,
        #endregion
        Relations = 6,
        RessourcesSubjects = 134,
        RessourcesTeachers = 135,
        Rooms = 2, //Rum
        RoomsAnswer = 24,
        RoomsFiles = 130,
        RoomsBulletin = 146,
        Schedule = 3, //Skema
        ScheduleTemplatesView = 63,
        Screen = 89,
        ScreenScrollText = 90,
        SMS = 29,
        Specialarrangements = 103,
        Subject = 167,
        SuperAdmin = -1,
        Substitutes = 174,
        Translation = 163,
        Users = 27, //Users - SUB
        UserType = 126,
        #region Show on user
        ViewProfileAbsence = 77,
        ViewProfileAdditional = 73,
        ViewProfileAddress = 67,
        ViewProfileBirthday = 72,
        ViewProfileContactPhone = 71,
        ViewProfileCountry = 81,
        ViewProfileCPR = 85,
        ViewProfileDescription = 112,
        ViewProfileDiaryPosts = 83,
        ViewProfileDiaryPostsComments = 130,
        ViewProfileEmail = 84,
        ViewProfileFiles = 205,

        ViewProfileFormularAnswers = 107,
        ViewProfileGroups = 79,
        ViewProfileInitials = 110,
        ViewProfileMobile = 70,
        ViewProfileGrade = 143,
        ViewProfileNote = 80,
        ViewProfilePhone = 69,
        ViewProfileProfileImage = 76,
        ViewProfileProtectedData = 104,
        ViewProfileRelations = 78,
        ViewProfileRoom = 74,
        ViewProfileSchoolline = 151,
        ViewProfileSex = 87,
        ViewProfileStudentId = 86,
        ViewProfileUnicUser = 82,
        ViewProfileWebsite = 75,
        ViewProfileWorkPhone = 109,
        ViewProfileZipAndCity = 68,
        ViewProfileInjury = 169,
        #endregion
        ViggoCms = 56,
        ViggoCrm = 152,
        ViggoInfo = 57,
        ViggoViggo = 58,
        ViggoSchedulePlanning = 166,
        ViggoOrder = 201,


        //Højeste nummer er pt 207

    }
    public enum NotificationSettings
    {
        Notification = 0,
        Push = 1,
        Mail = 2,
    }

    public enum ViggoScreenFunction
    {
        Bulletin = 0,
        Schedule = 1,
        Text = 2,
        Picture = 3,
        Slideshow = 4,
        Userdata = 5,
        RssFeeds = 6,
        TimeDate = 7,
        Substitute = 8,
        Absence = 9,
    }
    public enum ViggoElementSettings
    {
        Name,
        AutoScroll,

        BulletinId,

        ScheduleId,
        ScheduleDay,


        TypeId,
        UsertypeId,

        Userimages,

        FolderId,

        RssType,
        RssCount,
        RssTypeDmi,
        RssTypeDmiZipcode,
        RssTypeDrLink,
        RssTypeFacebookLink,
        RssTypeTwitterLink,

        RolleId,

        Textboks
    }
    public enum ViggoCheckInOutSecurity
    {
        LastTwoInCpr = 1,
        Birthday = 2,
        Studentid = 3,
    }
    public enum ViggoCheckInOutTypes
    {
        CheckIn = 1,
        CheckOut = 2,
        CheckInAndOut = 3,
        CheckOutAndIn = 4,
    }


    public enum ViggoScreenRss
    {
        DMI = 1,
        DR = 2,
        Facebook = 3,
        Twitter = 4,
    }
    public enum ViggoScreenEffects
    {
        Fade = 0,
        SlideUp = 1,
        SlideDown = 2,
        SlideRight = 3,
        SlideLeft = 4,
        FadeOutDown = 5,
        ScrollLeft = 6,
        FadeSwitch = 7
    }


    public enum ViggoScreenThemes
    {
        BlackWhiteDark,
        BlackWhiteLight,
        GreyDark,
        Momondo,
        Blue,
    }

    public enum ViggoJobs
    {
        Mail = 0,
        SecureMail = 2,
        Delete = 1,
        Notifications = 3,
    }
    public enum GroupImport
    {
        Cpr,
        Studentid
    }



    public enum UpdateStatus
    {
        ErrorLogRegistered = 0,
        ErrorRegistered = 1,
        ErrorSolved = 2,
        FeaturePlanned = 10,
        DuringDevelopment = 11,
        NewFeature = 12,
        SimpleUpdate = 20,
        ModuleUpdate = 21,
        SystemUpdate = 22,
        OperatingStatusOk = 30,
        OperatingStatusDowntime = 31,
        OperatingStatusUnstable = 32,
    }
    public enum Modulacl
    {
        Viggo = 0,
        Info = 1,
        Cms = 2,
        SchedulePlanning = 3,
    }
    public enum ViggoCmsElementsType
    {
        Calender = 0,

    }
    public enum ViggoCmsImageType
    {
        Gallery = 0,
        Topbar = 1,
        News = 2
    }

    public enum ViggoFunctionSub
    {
        AccesPages = 1,
        AccesPost = 2,
        AccesFiles = 3,
        AccesMember = 4,
        AccesBulletin = 5,
        AccesAbsenceDiaryCalendar = 6,
    }
    public enum ClientFunctionAcl
    {
        AccessCprLogin = 4,
        AccessEmailLogin = 5,
        AccessInitialsLogin = 28,
        AccessStudentidLogin = 7,
        AccessUnicLogin = 6,
        CmsDescriptionhp = 22,
        CmsElements = 26,
        CmsGallerythumbs = 23,
        CmsGuestbook = 19,
        CmsHighlights = 20,
        CmsNews = 21,
        CmsNewsmail = 25,
        CmsSignup = 27,
        CmsTopbar = 24,
        CmsWebshop = 18,
        InfoDiploma = 10,
        InfoScreen = 9,
        ModuleInjury = 30,
        ModuleCms = 2,
        ModuleInfo = 3,
        ModuleSchedulePlanning = 29,
        ModuleViggo = 1,
        SystemClientlogo = 8,
        ViggoAbsence = 15,
        ViggoAppearance = 16,
        ViggoStudentcpr = 13,
        ViggoStudentid = 11,
        ViggoStudentroom = 12,
        ViggoUsernote = 14,

        //Højeste nummer er pt 30
    }
    public enum ViggoFilSharingAuthorization
    {
        Read = 1,
        Upload = 2,
        CreateFolder = 3,
        DeleteUpdate = 4,
        GrantPermission = 5,

    }
    public enum ViggoGroupType
    {
        Group = 0,
        Edbbrugs = 1,
        Grade = 2,
    }
    public enum InjuryType
    {
        HeadFront = 1,
        NeckFront = 2,
        HandLeftFront = 3,
        HandRightFront = 4,
        LowerArmLeftFront = 5,
        LowerArmRightFront = 6,
        ElbowLeftFront = 7,
        ElbowRightFront = 8,
        UpperArmLeftFront = 9,
        UpperArmRightFront = 10,
        ShoulderLeftFront = 11,
        ShoulderRightFront = 12,
        FrontUpperArea = 13,
        FrontLowerArea = 14,
        HipLeftFront = 15,
        HipRightFront = 16,
        UpperLegLeftFront = 17,
        UpperLegRightFront = 18,
        KneeLeftFront = 19,
        KneeRightFront = 20,
        LowerLegLeftFront = 21,
        LowerLegRightFront = 22,
        AnkleLeftFront = 23,
        AnkleRightFront = 24,
        ForefootLeft = 25,
        ForefootRight = 26,

        HeadBack = 100,
        NeckBack = 101,
        HandLeftBack = 102,
        HandRightBack = 103,
        LowerArmLeftBack = 104,
        LowerArmRightBack = 105,
        ElbowLeftBack = 106,
        ElbowRightBack = 107,
        UpperArmLeftBack = 108,
        UpperArmRightBack = 109,
        ShoulderLeftBack = 110,
        ShoulderRightBack = 111,
        BackUpperArea = 112,
        BackLowerArea = 113,
        HipLeftBack = 114,
        HipRightBack = 115,
        UpperLegLeftBack = 116,
        UpperLegRightBack = 117,
        KneeLeftBack = 118,
        KneeRightBack = 119,
        LowerLegLeftBack = 120,
        LowerLegRightBack = 121,
        AnkleLeftBack = 122,
        AnkleRightBack = 123,
        HealArchLeft = 124,
        HealArchRight = 125
    }
    public enum ViggoFormularStatus
    {
        Nothing = 0,
        Accepted = 1,
        Wait = 2,
        Closed = 3,
    }
    /// <summary>
    /// Helperclass for parsing ViggoFunction Enum's
    /// </summary>
    public class ViggoFunctionParser
    {
        /// <summary>
        /// Parses an int FunctionId to an instance of ViggoFunction enum
        /// </summary>
        /// <param name="functionId"></param>
        /// <returns></returns>
        public static ViggoFunction Parse(int functionId)
        {
            try
            {
                return (ViggoFunction)Enum.Parse(typeof(ViggoFunction), functionId.ToString());
            }
            catch
            {
                throw new Exception("ViggoFunction could not be typecasted to enum from int : \"" + functionId + "\"");
            }
        }
    }

    public enum ViggoUserSettingsFunction
    {
        InfoSchoolYears = 1,
        InfoElementsView = 2,
        InfoScheduleView = 3,
        ViggoActivityType = 4,
        CrmSettings = 149,
        SchoolYears = 139,
        Diary = 32,
        CrmStatus = 155,
        CrmType = 154,
        ScheduleTemplatesView = 63,
        AbsenceCauses = 96,
        AbsencePlace = 95,
        Substitutes = 174

    }
}
