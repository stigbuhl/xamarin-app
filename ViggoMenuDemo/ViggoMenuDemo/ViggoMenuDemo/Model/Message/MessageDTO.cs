﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Filesharing;
using ViggoMenuDemo.Model.Interfaces;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Message
{
    public class MessageDTO : IListableObject
    {
        public int Id { get; set; }
        public int AnswerId { get; set; }
        public DateTime? Timestamp { get; set; }
        public string Headline { get; set; }
        public string Contents { get; set; }
        public List<UserDTO> Users { get; set; }
        public int CountReceivers { get; set; }
        public int NewMessage { get; set; }
        public int RefUserMessagesId { get; set; }
        public bool AreThereFiles { get; set; }
        public List<FileDTO> Files { get; set; }
        public List<int> Answer { get; set; }
        public int CountAnswer { get; set; }
        public List<MessageDTO> Answers { get; set; }
        public UserDTO FromUser { get; set; }
        public int Updated_by { get; set; }
        public int FolderId { get; set; }
        public bool HasBeenAnswered { get; set; }

        public string FullName
        {
            get
            {
                var s = "";
                s = FromUser.FullName;
                return s;
            }
        }

        public String ImageSmall
        {
            get
            {
                var s = "";
                s = FromUser.ImageBig;
                Uri uri = new Uri(s);
                return s;
            }
        }
    }
}
