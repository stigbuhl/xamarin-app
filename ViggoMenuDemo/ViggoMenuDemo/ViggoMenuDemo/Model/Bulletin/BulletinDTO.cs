﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Filesharing;
using ViggoMenuDemo.Model.Room;
using ViggoMenuDemo.Model.Screen;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Bulletin
{
    public class BulletinDTO
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public RoomDTO Room { get; set; }
        public string Icon { get; set; }
        public Nullable<DateTime> TimeStart { get; set; }
        public Nullable<DateTime> TimeEnd { get; set; }
        public int Updated_by { get; set; }
        public Nullable<DateTime> Timestamp { get; set; }
        public UserDTO UpdatedbyUser { get; set; }
        public string Headline { get; set; }
        public string Contents { get; set; }
        public List<int> UserTypesIds { get; set; }
        public List<UserTypeDTO> UserTypes { get; set; }
        public List<int> ScreensIds { get; set; }
        public List<ScreenDTO> Screens { get; set; }
        public string Bgcolor { get; set; }
        public int BulletinUserType { get; set; }
        public List<FileDTO> Files { get; set; }

    }
}
