﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViggoMenuDemo.Model.Bulletin
{
    public class BulletinBoardDTO
    {
        public List<BulletinDTO> BulletinList { get; set; }
        public List<BulletinDTO> HiddenBulletins { get; set; }
        public string UserTypeName { get; set; }
    }
}
