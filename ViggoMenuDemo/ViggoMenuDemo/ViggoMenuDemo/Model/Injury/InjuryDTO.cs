﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Filesharing;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Injury
{
    public class InjuryDTO
    {
        public int Id { get; set; }
        public string UserDescription { get; set; }
        public string Description { get; set; }
        public InjuryType InjuryType { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime? ExpectedEnd { get; set; }
        public string TherapistNote { get; set; }
        public string PhysicalLimitations { get; set; }
        public bool WishesTreatment { get; set; }
        public bool TreatmentAssigned { get; set; }
        public sbyte Level { get; set; }
        public sbyte? Nrs { get; set; }
        public int? TherapistUserId { get; set; }
        public UserDTO TherapistUser { get; set; }
        public int UserId { get; set; }
        public UserDTO User { get; set; }
        public UserDTO UpdatetBy { get; set; }
        public DateTime Timestamp { get; set; }
        public InjuryDTO LastFollowUp { get; set; }
        public int CountFollowUp { get; set; }
        public UserDTO MadeBy { get; set; }
        public DateTime MadeDate { get; set; }
        public List<InjuryDTO> AllFollowUps { get; set; }
        public InjuryAccessDTO Access { get; set; }
        public List<InjuryRecordDTO> AllTherapistRecord { get; set; }
        public List<FileDTO> Files { get; set; }
    }

    public class InjuryAccessDTO
    {
        public bool AccessTherapist { get; set; }
        public bool AccessBasic { get; set; }
        public bool AccessAdvanced { get; set; }
        public bool AccessOwnBasic { get; set; }
        public bool AccessOwnAdvanced { get; set; }
        public bool AccessAreThereTherapist { get; set; }
    }

    public class InjuryRecordDTO
    {
        public int Id { get; set; }
        public int InjuryId { get; set; }
        public InjuryType InjuryType { get; set; }
        public DateTime? ExpectedEnd { get; set; }
        public string TherapistNote { get; set; }
        public string PhysicalLimitations { get; set; }
        public sbyte Level { get; set; }
        public sbyte? Nrs { get; set; }
        public string Description { get; set; }
    }

    public class InjuryUserDTO
    {
        public UserDTO User { get; set; }
        public List<InjuryDTO> Injurys { get; set; }
    }

    public class dbInjuryUser
    {
        public int InjuryId { get; set; }
        public int InjuryTypeId { get; set; }
        public sbyte Level { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime? ExpectedEnd { get; set; }
        public string Description { get; set; }
        public string UserDescription { get; set; }
        public bool WishesTreatment { get; set; }
        public bool TreatmentAssigned { get; set; }
        public string PhysicalLimitations { get; set; }
        public int UserById { get; set; }
        public string UserByFirstName { get; set; }
        public string UserByMiddlename { get; set; }
        public string UserByLastname { get; set; }
        public string UserByImage { get; set; }
        public int InjuryJournalId { get; set; }
        public sbyte InjuryJournalLevel { get; set; }
        public DateTime? InjuryJournalExpectedEnd { get; set; }
        public string InjuryJournalDescription { get; set; }
        public string InjuryJournalPhysicalLimitations { get; set; }
        public sbyte? InjuryJournalNrs { get; set; }
        public string InjuryJournalTherapistNote { get; set; }
        public int InjuryJournalUpdatedById { get; set; }
        public string InjuryJournalUpdatedByFirstName { get; set; }
        public string InjuryJournalUpdatedByMiddlename { get; set; }
        public string InjuryJournalUpdatedByLastname { get; set; }
        public string InjuryJournalUpdatedByImage { get; set; }
        public DateTime InjuryJournalTimestamp { get; set; }
        public int CountFollowUp { get; set; }
        public int MadeById { get; set; }
        public string MadeByFirstName { get; set; }
        public string MadeByMiddlename { get; set; }
        public string MadeByLastname { get; set; }
        public string MadeByImage { get; set; }
        public DateTime MadeDate { get; set; }
        public DateTime Timestamp { get; set; }
    }
    public class dbInjuryJournal
    {
        public int Id { get; set; }
        public int InjuryId { get; set; }
        public sbyte Level { get; set; }
        public DateTime? ExpectedEnd { get; set; }
        public string Description { get; set; }
        public string PhysicalLimitations { get; set; }
        public sbyte? Nrs { get; set; }
        public string TherapistNote { get; set; }
        public int UserById { get; set; }
        public string UserByFirstName { get; set; }
        public string UserByMiddlename { get; set; }
        public string UserByLastname { get; set; }
        public string UserByImage { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
