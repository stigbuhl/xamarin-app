﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Calendar;
using ViggoMenuDemo.Model.Filesharing;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.SchedulePlanning.Elements
{

    #region Dashboard Element
    //public class ScheduleDashboardElementDTO
    //{
    //    public int SchoolYearId { get; set; }
    //    public FolderDTO Folders { get; set; }
    //    public int SelectedFolderId { get; set; }
    //    public string SelectedFolderName { get; set; }
    //    public List<ScheduleElementContentDTO> Content { get; set; }
    //}
    //public class ScheduleElementContentDTO
    //{
    //    public ScheduleElementDTO ScheduleElement { get; set; }
    //    public double SettledHours { get; set; } //afviklet
    //    public double PlannedHours { get; set; } //fremtiden

    //}
    //public class ScheduleDashboardElementDetailsDTO
    //{
    //    public ScheduleElementDTO ScheduleElement { get; set; }
    //    public List<WorkTimeCountingDetailsDTO> Published { get; set; }
    //    public UpdatedTo UpdatedTo { get; set; }
    //    public List<UserDTO> Employees { get; set; }
    //    public bool ShowUpdatedTo { get; set; }
    //}
    //#endregion
    //#region Dashboard Employee
    //public class ScheduleDashboardEmployeeDTO
    //{
    //    public SchoolYearDTO SchoolYear { get; set; }
    //    public DateTime DateStart { get; set; }
    //    public DateTime DateEnd { get; set; }
    //    public List<ScheduleEmployeeContentDTO> Content { get; set; }
    //}
    //public class ScheduleEmployeeContentDTO
    //{
    //    public UserDTO Employee { get; set; }
    //    public int NetTime { get; set; }
    //    public int Settled { get; set; }
    //    public List<WorkTimeCountingDetailsDTO> Published { get; set; }
    //}
    #endregion



    //public class ScheduleSetupElementDTO
    //{
    //    public int SchoolYearId { get; set; }
    //    public FolderDTO Folders { get; set; }
    //    public int SelectedFolderId { get; set; }
    //    public string SelectedFolderName { get; set; }
    //    public FolderAndContentsDTO SubFoldersAndContent { get; set; }
    //}

    //public class ScheduleSetupElementDetailsDTO
    //{
    //    public int Id { get; set; }
    //    public ViggoActivityType Type { get; set; }
    //    public string Name { get; set; }
    //    public List<int> UsersIds { get; set; }
    //    public List<UserDTO> Users { get; set; }
    //    public List<int> GroupsIds { get; set; }
    //    public List<GroupDTO> Groups { get; set; }
    //    public List<int> GroupsAccessIds { get; set; }
    //    public List<ScheduleElementDTO> Elements { get; set; }
    //    public List<int> ElementsIds { get; set; }

    //    public List<GroupDTO> GroupsAccess { get; set; }
    //    public List<int> AccessSwapIds { get; set; }
    //    public List<GroupDTO> GroupsAccessSwap { get; set; }
    //    public DateTime Timestart { get; set; }
    //    public DateTime Timeend { get; set; }
    //    public int SchoolYearId { get; set; }
    //    public int? TeachingHours { get; set; }
    //    public int? Preparation { get; set; }
    //    public int? MaxCounting { get; set; }
    //    public int? MinCounting { get; set; }
    //    public bool ShowSchedule { get; set; }
    //    public bool ShowInBottom { get; set; }
    //    public int FolderId { get; set; }
    //    public string Note { get; set; }
    //    public int? OwnPresenceInViggo { get; set; }
    //    public FolderDTO Folders { get; set; }
    //    public string BgColor { get; set; }
    //    public int? DefaultPremiseId { get; set; }
    //    public PremisesDTO DefaultPremise { get; set; }
    //    public int DefaultTime { get; set; }
    //    public TimeSpan DefaultTimeStart { get; set; }
    //    public int UserId { get; set; }
    //    public List<ExceptionsDTO> Exceptions { get; set; }
    //    public SchoolYearDTO SchoolYear { get; set; }
    //    public int CountingProcent { get; set; }
    //    public int ActivityId { get; set; }
    //    public bool UpdatedHourIfSwapped { get; set; }
    //}

    public class ScheduleElementDTO
    {
        public int Id { get; set; }
        public ViggoActivityType Type { get; set; }
        public string Name { get; set; }
        public bool Selectet { get; set; }
        public FolderDTO Folder { get; set; }
        public int DefaultTime { get; set; }
        public string Note { get; set; }
        public int TeachingHours { get; set; }
        public int SchoolYearId { get; set; }
        public string BgColor { get; set; }
        public List<ScheduleElementDTO> TaskElement { get; set; }
        public int AccessLevel { get; set; } //0 = edit, 1=swap
    }

    public enum ViggoActivityType
    {
        Subject = 0,
        Activity = 1,
        Presence = 2,
        Task = 3,
        Preparation = 4,
        Supervision = 5
    }

    //public class SchedulePlanningTemplatesItem
    //{
    //    public int Id { get; set; }
    //    public int ScheduleTemplatesId { get; set; }
    //    public int ActivityId { get; set; }
    //    public ViggoActivityType Type { get; set; }
    //    public string Headline { get; set; }
    //    public List<PremisesDTO> Premises { get; set; }
    //    public List<GroupDTO> Groups { get; set; }
    //    public List<int> GroupsUsers { get; set; }
    //    public List<UserDTO> Users { get; set; }
    //    public DateTime TimeStart { get; set; }
    //    public DateTime TimeEnd { get; set; }
    //    public DateTime TimeStamp { get; set; }

    //    public int? ActivityTeachingHours { get; set; }
    //    public int? ActivityPreparation { get; set; }
    //    public int? ActivityMaxCounting { get; set; }
    //    public int? ActivityMinCounting { get; set; }
    //    public DateTime ActivityTimestart { get; set; }
    //    public DateTime ActivityTimeend { get; set; }
    //    public bool RemoveItem { get; set; }
    //}

    //public class ScheduleActivityExceptionsDTO
    //{
    //    public ScheduleSetupElementDetailsDTO ScheduleActivityDetails { get; set; }
    //    public ExceptionsDTO Exceptions { get; set; }
    //}
    public class UserExceptionActivity
    {
        public int ActivityId { get; set; }
        public DateTime Timestart { get; set; }
        public DateTime Timeend { get; set; }
        public int? NetTime { get; set; }
        public int Degree { get; set; }
        public int? Preparation { get; set; }
        public int? MaxCounting { get; set; }
        public int? MinCounting { get; set; }
        public int UserId { get; set; }
        public int CountingProcent { get; set; }
    }
}
