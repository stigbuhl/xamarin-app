﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.SchedulePlanning.Elements;

namespace ViggoMenuDemo.Model.SchedulePlanning.Schedule
{
    public class SchedulesDTO
    {
        public List<ScheduleDTO> Schedules { get; set; }
        public SchedulesDetailsDTO SchedulesDetail { get; set; }
    }
    public class ScheduleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SchoolYearId { get; set; }
        public int Length { get; set; }
        public bool Edit { get; set; }
        public bool Publish { get; set; }
    }
    public class SchedulesDetailsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SchoolYearId { get; set; }
        public string Description { get; set; }
        public int Length { get; set; }
        public PeriodDTO Period { get; set; }
        public List<SchedulesDatesDTO> Exceptions { get; set; }
        public List<ScheduleElementDTO> Activity { get; set; }
    }
    public class SchedulesDatesDTO
    {
        public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }
    }

    public enum ScheduleAction
    {
        HourCounting,
        Conflict,
        Publish,

    }

    public class ShowSchedulesDTO
    {
        public bool Subject { get; set; }
        public bool Activity { get; set; }
        public bool Preparation { get; set; }
        public bool Presence { get; set; }
        public bool Task { get; set; }
        public bool Supervision { get; set; }
    }
}
