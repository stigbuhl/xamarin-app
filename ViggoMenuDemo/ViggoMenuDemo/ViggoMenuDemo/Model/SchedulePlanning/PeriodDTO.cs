﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.SchedulePlanning
{
    public class PeriodDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<PeriodDatesDTO> Dates { get; set; }
        public List<SchedulePlanning.Schedule.ScheduleDTO> AttachedSchedules { get; set; }
        public int FolderId { get; set; }
        public int SchoolYearId { get; set; }
        public UserDTO UpdatedByUser { get; set; }
        public DateTime? Timestamp { get; set; }
        public DateTime _StartDateTime { get; set; }
        public DateTime _EndDateTime { get; set; }
        public DateTime StartDateTime
        {
            get
            {
                if (Dates == null || Dates.Count == 0)
                {
                    return _StartDateTime;
                }
                return Dates.Min(t => t.Timestart);
            }
            set
            {
                _StartDateTime = value;
            }
        }

        public DateTime EndDateTime
        {
            get
            {
                if (Dates == null || Dates.Count == 0)
                {
                    return _EndDateTime;
                }
                return Dates.Max(t => t.Timeend);
            }
            set
            {
                _EndDateTime = value;
            }
        }
    }
    public class PeriodDatesDTO
    {
        public DateTime Timestart { get; set; }
        public DateTime Timeend { get; set; }

    }
}
