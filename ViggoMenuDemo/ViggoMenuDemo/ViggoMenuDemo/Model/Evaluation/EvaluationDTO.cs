﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Evaluation
{
    public class EvaluationDTO
    {
        public int Id { get; set; }
        public int Updated_by { get; set; }
        public DateTime? Timestamp { get; set; }
        public string Contents { get; set; }
        public string Headline { get; set; }
        public bool Visible { get; set; }
        public int FolderId { get; set; }
        public List<EvaluationsUserDTO> AddressedUsers { get; set; }
        public List<GroupDTO> AddressedGroup { get; set; }
    }

    public class EvaluationUserViewDTO
    {
        public int EvaluationId { get; set; }
        public int AnswerId { get; set; }
        public string Headline { get; set; }
        public DateTime? Timestamp { get; set; }
        public int UserId { get; set; }
        public string Contents { get; set; }
        public int RefEvaluateAnswerId { get; set; }
        public int FolderId { get; set; }
        public int Refgroupevaluateid { get; set; }
        public bool IsPublicForRelation { get; set; }

    }
    public class EvaluationEditAnswerViewDTO
    {
        public int EvaluationId { get; set; }
        public int AnswerId { get; set; }
        public string Headline { get; set; }
        public string Contents { get; set; }
        public int RefEvaluateAnswerId { get; set; }
        public int Refgroupevaluateid { get; set; }
        public int UserId { get; set; }
        public UserDTO User { get; set; }
        public int FolderId { get; set; }
        public int? LockedBy { get; set; }
        public DateTime? Lockeddate { get; set; }
        public UserDTO UpdatetBy { get; set; }
        public DateTime Timestamp { get; set; }
        public bool PublicRelations { get; set; }
        public List<EvaluationComment> EvaluationComment { get; set; }
    }

    public class EvaluationComment
    {
        public int Id { get; set; }
        public int EvaluateAnswerId { get; set; }
        public UserDTO UpdatetBy { get; set; }
        public DateTime Timestamp { get; set; }
        public string Contents { get; set; }
    }
    public class EvaluationsUserDTO
    {
        public UserDTO User { get; set; }
        public bool Answer { get; set; }
        public int RefEvaluateAnswerId { get; set; }
        public int RefGroupEvaluateId { get; set; }
        public UserDTO LockedBy { get; set; }
        public DateTime? Lockeddate { get; set; }
        public UserDTO UpdatetBy { get; set; }
        public DateTime Timestamp { get; set; }
        public bool PublicRelations { get; set; }
    }

    public class dbEvaluationsRelations
    {
        public int UserId { get; set; }
        public int EvaluationId { get; set; }
        public string EvaluationHeadline { get; set; }
        public int EvaluationAnswerId { get; set; }
    }
}
