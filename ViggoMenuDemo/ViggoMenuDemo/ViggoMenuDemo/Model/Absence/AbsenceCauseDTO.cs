﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Absence
{
    public class AbsenceCauseDTO
    {
        public int Id { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime? Timestamp { get; set; }
        public string Cause { get; set; }
        public int HoursADay { get; set; }
        public int? Orderby { get; set; }

        public virtual ICollection<AbsenceDTO> UsersAbsence { get; set; }
    }
}
