﻿using System;
using System.Collections.Generic;
using ViggoMenuDemo.Model.Absence;

namespace ViggoMenuDemo.Model.User
{
    public class AbsenceDTO
    {
        public int Id { get; set; }
        public bool Deleted { get; set; }
        public int Updated_by { get; set; }
        public DateTime? Timestamp { get; set; }
        public int Userid { get; set; }

        //mapped boolean from sbyte
        public bool Arrived { get; set; }

        public DateTime? Timestart { get; set; }
        public DateTime? Timeend { get; set; }
        public int Place_id { get; set; }
        public int Cause_id { get; set; }
        public string Note { get; set; }
        public bool Active { get; set; }

        public int Madeby { get; set; }
        public DateTime MadeTime { get; set; }
        public virtual AbsenceCauseDTO AbsenceCause { get; set; }
        public virtual AbsencePlaceDTO AbsencePlace { get; set; }
        //public virtual UserDTO MadebyUser { get; set; }
        public virtual UserDTO User { get; set; }
        public virtual UserDTO UpdatedbyUser { get; set; }
        public List<AbsenceComment> AbsenceComment { get; set; }
        public int CommentCount { get; set; }
    }
    public class dbAbsenceRelations
    {
        public int UserId { get; set; }
        public int AbsenceId { get; set; }
        public string AbsenceNote { get; set; }
        public DateTime AbsenceTimestart { get; set; }
        public DateTime AbsenceTimeend { get; set; }
        public DateTime AbsenceTimestamp { get; set; }
        public string CauseName { get; set; }
        public string PlaceName { get; set; }
        public int UpdatedbyUserId { get; set; }
        public string UpdatedbyUserFirstname { get; set; }
        public string UpdatedbyUserMiddlename { get; set; }
        public string UpdatedbyUserLastname { get; set; }
        public string UpdatedbyUserImage { get; set; }
    }
    public class AbsenceComment
    {
        public int Id { get; set; }
        public int AbsenceUserId { get; set; }
        public UserDTO UpdatetBy { get; set; }
        public DateTime Timestamp { get; set; }
        public string Contents { get; set; }
    }

    public class AbsenceForConflicts
    {
        public int UserId { get; set; }
        public List<AbsenceForConflictsDates> Absence { get; set; }
    }

    public class AbsenceForConflictsDates
    {
        public DateTime Timestart { get; set; }
        public DateTime Timeend { get; set; }
    }
}