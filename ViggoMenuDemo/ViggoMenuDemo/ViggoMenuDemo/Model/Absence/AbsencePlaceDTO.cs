﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Absence
{
    public class AbsencePlaceDTO
    {

        public int Id { get; set; }
        public int UpdatedBy { get; set; }
        public Nullable<DateTime> Timestamp { get; set; }
        public string Place { get; set; }
        public sbyte OtherLocation { get; set; }
        public int? Orderby { get; set; }
        public virtual ICollection<AbsenceDTO> UsersAbsence { get; set; }
    }
}
