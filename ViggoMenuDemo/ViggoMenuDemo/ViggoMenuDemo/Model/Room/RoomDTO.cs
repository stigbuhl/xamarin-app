﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Relations;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Room
{
    /// <summary>
    /// Data Transfer Object, which is 
    ///  used to get the rooms the user is entitled to see
    /// </summary>
    public class RoomDTO
    {
        //   List<ViggoFunctionSubRoom> _subFunctions = new List<ViggoFunctionSubRoom>(); 
        /// <summary>
        /// Id of the room
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name of the room
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Short description of the room
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The user last updated the room
        /// Id & image & Fullname fra user
        /// </summary>
        public int Updated_By { get; set; }
        /// <summary>
        /// Last changed date
        /// </summary>
        public DateTime? TimeStamp { get; set; }
        /// <summary>
        /// The path string of the icon for the room
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// The path string of groups for the room
        /// </summary>
        public string GroupsIds { get; set; }
        public List<ViggoFunctionSubRoom> RoomSubFunctionsCurentuser { get; set; }
        public List<ViggoFunctionSubRoom> RoomSubFunctionsEdit { get; set; }
        public List<RelationsDTO> Relations { get; set; }
    }

    public class ViggoFunctionSubRoom
    {
        //public AuthorizationLevel AuthorizationLevel { get; set; }
        //public string StringGroups { get; set; }
        //public List<GroupDTO> Groups { get; set; }
        //public ViggoFunctionSub Functionsub { get; set; }
        //public List<int> RelationIds { get; set; }
        ////public AuthorizationLevel AccesAbsenceDiaryCalendar { get; set; }
        ////public string ShowGroupsAbsenceDiaryCalendar { get; set; }
        ////public AuthorizationLevel AccesPages { get; set; }
        ////public string AccesPagesGroups { get; set; }
        ////public AuthorizationLevel AccesPost { get; set; }
        ////public string AccesPostGroups { get; set; }
        ////public AuthorizationLevel AccesPostRelations { get; set; }
        ////public string AccesPostRelationsGroups { get; set; }
        ////public List<int> AccesPostRelationIds { get; set; }
        ////public AuthorizationLevel AccesFiles { get; set; }
        ////public string AccesFilesGroups { get; set; }
        ////public AuthorizationLevel AccesMember { get; set; }
        ////public string AccesMemberGroups { get; set; }
        ////public List<GroupDTO> Groups { get; set; }
    }

    //public class RoomFunctionsDTO
    //{
    //    public int Functionsubid { get; set; }
    //    public int Editright { get; set; }
    //    public string Groups { get; set; }
    //    public int? RelationsId { get; set; }
    //}

    public class RoomHomeDTO
    {
        public int Id { get; set; }
        public int? Answerid { get; set; }
        public string Headline { get; set; }
        public string Contents { get; set; }
        public bool Readed { get; set; }
        public int unread { get; set; }
        public string RoomName { get; set; }
        public string Icon { get; set; }
        public DateTime Timestamp { get; set; }
        public UserDTO UpdatedByUser { get; set; }
        private string Firstname { get; set; }
        private string Middlename { get; set; }
        private string Lastname { get; set; }
        private string Image { get; set; }
        private int UserId { get; set; }
    }
    public class AccessToRoomDTO
    {
        public List<int> ForumGroupsRead { get; set; }
        public List<int> ForumGroupsReadRelations { get; set; }
        public List<int> RelationIdsForumRead { get; set; }
        public List<int> ForumGroupsWrite { get; set; }
        public List<int> ForumGroupsWriteRelations { get; set; }
        public List<int> RelationIdsForumWrite { get; set; }

        public List<int> MemberGroupsRead { get; set; }
        public List<int> MemberGroupsReadRelations { get; set; }
        public List<int> RelationIdsMemberRead { get; set; }

        public List<int> PageGroupsRead { get; set; }
        public List<int> PageGroupsReadRelations { get; set; }
        public List<int> RelationIdsPageRead { get; set; }
        public List<int> PageGroupsWrite { get; set; }
        public List<int> PageGroupsWriteRelations { get; set; }
        public List<int> RelationIdsPageWrite { get; set; }

        public List<int> FileSharingGroupsRead { get; set; }
        public List<int> FileSharingGroupsReadRelations { get; set; }
        public List<int> RelationIdsFileSharingRead { get; set; }
        public List<int> FileSharingGroupsWrite { get; set; }
        public List<int> FileSharingGroupsWriteRelations { get; set; }
        public List<int> RelationIdsFileSharingWrite { get; set; }

        public List<int> BulletinGroupsRead { get; set; }
        public List<int> BulletinGroupsReadRelations { get; set; }
        public List<int> RelationIdsBulletinRead { get; set; }
        public List<int> BulletinGroupsWrite { get; set; }
        public List<int> BulletinGroupsWriteRelations { get; set; }
        public List<int> RelationIdsBulletinWrite { get; set; }

        public List<int> AbsenceDiaryCalendarGroupsRead { get; set; }
    }

    public class dbUsersToMail
    {
        public int? Relationsid { get; set; }
        public int UserId { get; set; }
        public int? RelationsUserId { get; set; }
    }
}
