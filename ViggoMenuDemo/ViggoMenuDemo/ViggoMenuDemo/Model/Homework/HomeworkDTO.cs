﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Model.Calendar;
using ViggoMenuDemo.Model.Filesharing;
using ViggoMenuDemo.Model.User;

namespace ViggoMenuDemo.Model.Homework
{
    public class HomeworkDTO
    {
        public int HomeWorkId { get; set; }
        public string Headline { get; set; }
        public string Description { get; set; }
        public int NumberOfFiles { get; set; }
        public int Type { get; set; }
        public List<FileDTO> Files { get; set; }
        public CalendarAddEditDTO Calendar { get; set; }
        public DateTime Timestamp { get; set; }
        public UserDTO UpdatetUser { get; set; }
        public int CalenderId { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }
        public int MadeStatus { get; set; }
        public int? FolderId { get; set; }
        public FolderFilSharingAuthorizationDTO FolderFilSharingAccess { get; set; }
        public bool ShowDelivered { get; set; }
        public bool UserDelivered { get; set; }
        public bool UserAssignmentComment { get; set; }
        public bool UserEdit { get; set; }
        public List<AssigmentDeliveredDTO> HasDelivered { get; set; }
        public List<UserDTO> HasNotDelivered { get; set; }
        public AssignmentDTO Assignment { get; set; }
        public List<string> Groups { get; set; }
    }
    public class dbHomework
    {
        public int HomeworkId { get; set; }
        public DateTime Timestamp { get; set; }
        public string Headline { get; set; }
        public string Description { get; set; }
        public int NumberOfFiles { get; set; }
        public int Type { get; set; }
        public int CalendarId { get; set; }
        public string CalendarName { get; set; }
        public string CalendarNote { get; set; }
        public string GroupIds { get; set; }
        public bool MadeStatus { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime TimeEnd { get; set; }
        public int UserId { get; set; }
        public string UserFirstname { get; set; }
        public string UserMiddlename { get; set; }
        public string UserLastname { get; set; }
        public string UserImage { get; set; }
        public int? FolderId { get; set; }
    }
    public class AssigmentDeliveredDTO
    {

        public UserDTO User { get; set; }
        public AssignmentDTO Assignment { get; set; }
    }

    public class AssignmentDTO //ref_homework_users
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public List<FileDTO> Files { get; set; }
        public DateTime Timestamp { get; set; }
        public int HomeWorkId { get; set; }
        public int UserId { get; set; }
        public UserDTO User { get; set; }
        public int FolderId { get; set; }
        public AssignmentCommentDTO AssignmentComment { get; set; }
    }
    public class AssignmentCommentDTO //homework_comment
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public List<FileDTO> Files { get; set; }
        public DateTime Timestamp { get; set; }
        public UserDTO UpdatetUser { get; set; }
        public int AssignmentId { get; set; }
        public int FolderId { get; set; }
        public int HomeWorkId { get; set; }
        public int UserId { get; set; }
        public UserDTO User { get; set; }
        public bool ShowComment { get; set; }
    }
}
