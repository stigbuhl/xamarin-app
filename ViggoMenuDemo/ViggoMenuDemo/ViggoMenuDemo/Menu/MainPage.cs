﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using ViggoMenuDemo.Controller;
using ViggoMenuDemo.DetailPages;
using ViggoMenuDemo.DetailPages.Settings;
using ViggoMenuDemo.DetailPages.Start;
using ViggoMenuDemo.Menu;
using ViggoMenuDemo.Menu.MenuItems;


using Xamarin.Forms;

namespace ViggoMenuDemo
{
    public class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            Master = MenuController.Instance.GetMenuPage();
            Detail = new NavigationPage(new StartPage());
        }        
    }
}
