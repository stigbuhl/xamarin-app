﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViggoMenuDemo.Menu.MenuItems
{
    public class SubItem : MenuItem
    {
        public string SubAction { get; set; }
        public int? UsertypeSubId { get; set; }
        public bool ShowForAdmin { get; set; }
    }
}
