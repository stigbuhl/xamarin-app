﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace ViggoMenuDemo.Menu.MenuItems
{
    public class MenuItem : Button
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public List<SubItem> SubItems { get; set; }
        public string Class { get; set; }

        public void ToggleVisible()
        {
            foreach (var item in SubItems)
            {
                item.IsVisible = (item.IsVisible == true) ? item.IsVisible = false : item.IsVisible = true;
            }
        }

        public bool CanHideButtons()
        {
            bool response = false;

            if (SubItems != null)
            {
                response = true;
            }

            return response;
        }
    }
}
