﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using ViggoMenuDemo.Controller;
using ViggoMenuDemo.DetailPages;
using ViggoMenuDemo.DetailPages.Settings;
using ViggoMenuDemo.Menu.MenuItems;

using Xamarin.Forms;

namespace ViggoMenuDemo.Menu
{
    public class MenuPage : ContentPage
    {
        //Builds the actual leftside 'hamburger' menu
        public MenuPage()
        {
            Title = "Menu";
            Icon = "menu.png";

            ScrollView scrollView = new ScrollView();

            StackLayout stackLayout = new StackLayout
            {
                Spacing = 0,
                VerticalOptions = LayoutOptions.FillAndExpand
            };            
                        
            List<MenuItems.MenuItem> menuItems = MenuController.Instance.GetMenu();            

            foreach (var item in menuItems)
            {
                stackLayout.Children.Add(item);
                if (item.SubItems != null)
                {
                    foreach (var subItem in item.SubItems)
                    {
                        stackLayout.Children.Add(subItem);
                        subItem.IsVisible = false;
                    }
                }
            }

            if (Device.OS == TargetPlatform.iOS)
            {
                Padding = new Thickness(0, 40, 0, 0);
            }

            scrollView.Content = stackLayout;

            Content = scrollView;            
        }
    }
}
