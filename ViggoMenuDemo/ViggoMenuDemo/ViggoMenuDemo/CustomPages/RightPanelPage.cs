﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace ViggoMenuDemo.CustomPages
{
    public class RightPanelPage : ContentPage
    {
        private RelativeLayout _layout;
        private StackLayout _panel;

        //Add stacklayout to _layout
        public StackLayout FillLayout
        {
            set
            {
                _layout.Children.Add(value, Constraint.RelativeToParent((parent) =>
                {
                    return parent.X;
                }), Constraint.RelativeToParent((parent) =>
                {
                    return parent.Y;
                }), Constraint.RelativeToParent((parent) =>
                {
                    return parent.Width;
                }), Constraint.RelativeToParent((parent) =>
                {
                    return parent.Height;
                }));
                
                CreatePanel();
            }
        }
        
        public RightPanelPage()
        {
            _layout = new RelativeLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };            

            Content = _layout;

            //Add item to toolbar
            this.ToolbarItems.Add(new ToolbarItem
            {
                Text = "RightMenu",
                Icon = "menu.png",
                Order = ToolbarItemOrder.Primary,
                Command = new Command(o => {
                    AnimatePanel();
                }),
                Priority = 1,
            });
        }        

        private double _panelWidth = -1;
        /// <summary>
        /// Creates the right side menu panel
        /// </summary>
        private void CreatePanel()
        {
            if (_panel == null)
            {
                _panel = new StackLayout
                {
                    Children = {
                        new Label {
                            Text = "Options",
                            HorizontalOptions = LayoutOptions.Start,
                            VerticalOptions = LayoutOptions.Start,
                            XAlign = TextAlignment.Center,
                            TextColor = Color.White
                        },
                        new Button
                        {
                            Text = "Option 1",
                            Command = new Command (() =>
                            {
                                AnimatePanel();
                            })
                        },
                        new Button
                        {
                            Text = "Option 2",
                            Command = new Command (() =>
                            {
                                AnimatePanel();
                            })
                        },
                        new Button
                        {
                            Text = "Option 3",
                            Command = new Command (() =>
                            {
                                AnimatePanel();
                            })
                        },
                    },
                    Padding = 15,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    BackgroundColor = Color.FromRgba(0, 0, 0, 255)
                };

                // add to layout
                _layout.Children.Add(_panel,
                    Constraint.RelativeToParent((p) => {
                        return _layout.Width - (this.PanelShowing ? _panelWidth : 0);
                    }),
                    Constraint.RelativeToParent((p) => {
                        return 0;
                    }),
                    Constraint.RelativeToParent((p) => {
                        if (_panelWidth == -1)
                            _panelWidth = p.Width / 3;
                        return _panelWidth;
                    }),
                    Constraint.RelativeToParent((p) => {
                        return p.Height;
                    })
                );
            }
        }

        private bool _PanelShowing = false;
        /// <summary>
        /// Gets a value to determine if the panel is showing or not
        /// </summary>
        /// <value><c>true</c> if panel showing; otherwise, <c>false</c>.</value>
        private bool PanelShowing
        {
            get
            {
                return _PanelShowing;
            }
            set
            {
                _PanelShowing = value;
            }
        }

        /// <summary>
        /// Animates the panel in our out depending on the state
        /// </summary>
        private async void AnimatePanel()
        {

            // swap the state
            this.PanelShowing = !PanelShowing;

            // show or hide the panel
            if (this.PanelShowing)
            {
                // layout the panel to slide out
                var rect = new Rectangle(_layout.Width - _panel.Width, _panel.Y, _panel.Width, _panel.Height);
                await this._panel.LayoutTo(rect, 250, Easing.Linear);

                // scale in the children for the panel
                foreach (var child in _panel.Children)
                {
                    await child.ScaleTo(1, 50, Easing.Linear);
                }
            }
            else
            {
                // layout the panel to slide in
                var rect = new Rectangle(_layout.Width, _panel.Y, _panel.Width, _panel.Height);
                await this._panel.LayoutTo(rect, 200, Easing.Linear);
            }
        }
    }
}
