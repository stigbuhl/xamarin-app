﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using ViggoMenuDemo.CustomPages;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Functions
{
    public class Print : RightPanelPage
    {
        public Print()
        {
            FillLayout = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}
