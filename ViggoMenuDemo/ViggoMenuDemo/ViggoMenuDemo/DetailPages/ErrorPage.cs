﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.CustomPages;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages
{
    public class ErrorPage : RightPanelPage
    {
        public ErrorPage()
        {
            FillLayout = new StackLayout
            {
                Children = {
                    new Label { Text = "ErrorPage" }
                }
            };
        }
    }
}
