﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Controller;
using ViggoMenuDemo.CustomPages;
using ViggoMenuDemo.Model.Message;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Messages
{
    public class SelectedMessage : RightPanelPage
    {
        ObservableCollection<MessageDTO> messages = new ObservableCollection<MessageDTO>();

        public SelectedMessage(MessageDTO message)
        {
            GetMessageById(message);
            MessageDTO activeMessage = message;
            Label headlineLabel = new Label { Text = activeMessage.Headline, FontSize = 20 };
            Label fromUserLabel = new Label { Text = $"Afsender: {activeMessage.FromUser.FullName}" };
            Label contentsLabel = new Label
            {
                Text = activeMessage.Contents,
                HeightRequest = 100
            };
            Entry subjectEntry = new Entry { Placeholder = "Emne" };
            Editor answerBox = new Editor { Text = "Hej", HeightRequest = 100 };

            StackLayout stackLayout = new StackLayout
            {
                Padding = new Thickness(20, 20, 20, 20),
                Children = {
                    headlineLabel,
                    fromUserLabel,
                    new ScrollView
                    {
                        Content = new StackLayout
                        {
                            Children =
                            {
                                contentsLabel
                            }
                        }
                    },
                    subjectEntry,
                    answerBox,
                    new Button { Text = "Svar", Command = new Command( o =>
                            DoMessageSend(subjectEntry.Text != null ? subjectEntry.Text : subjectEntry.Placeholder, answerBox.Text, Convert.ToString(activeMessage.FromUser.Id), Convert.ToString(message.AnswerId))) },
                }
            };

            if (message.Answers != null)
            {
                if (message.Answers.Count < 0)
                {
                    foreach (var item in message.Answers)
                    {
                        messages.Add(item);
                    }
                }

            }

            ListView listView = new ListView
            {
                ItemsSource = messages,
                HasUnevenRows = true,

                ItemTemplate = new DataTemplate(() =>
                {

                    Label senderLabel = new Label();
                    senderLabel.SetBinding(Label.TextProperty, "Id");

                    Label subjectLabel = new Label();
                    subjectLabel.SetBinding(Label.TextProperty, "Headline");

                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            VerticalOptions = LayoutOptions.Center,
                            Spacing = 0,
                            Children =
                            {
                                senderLabel,
                                subjectLabel
                            },
                        }
                    };
                })

            };

            listView.ItemSelected += (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
                }

                MessageDTO selectedMessage = (MessageDTO)e.SelectedItem;

                headlineLabel.Text = selectedMessage.Headline;
                fromUserLabel.Text = $"Afsender: {selectedMessage.FromUser.FullName}";
                contentsLabel.Text = selectedMessage.Contents;

                activeMessage = selectedMessage;
            };

            stackLayout.Children.Add(listView);

            if (Device.OS == TargetPlatform.iOS)
            {
                stackLayout.Padding = new Thickness(20, 40, 20, 20);
            }

            FillLayout = stackLayout;
        }

        async Task GetMessageById(MessageDTO message)
        {
            HttpResponseMessage response;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string dest = $"http://{InfoController.Instance.GetDomain()}/rest/messages/get/{message.AnswerId}?token={InfoController.Instance.GetToken()}";

                response = await client.GetAsync(dest);

            }

            JObject messageInput = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            //message = JsonConvert.DeserializeObject<MessageDTO>(response.Content.ReadAsStringAsync().Result);
            message = messageInput.ToObject<MessageDTO>();

            if (message.Answers != null)
            {
                foreach (var item in message.Answers)
                {
                    messages.Add(item);
                }
            }
            if (messages.Count != 0)
            {
                messages.Add(message);
            }
        }

        async void DoMessageSend(string subject, string body, string to, string answerId)
        {
            await MessageController.Instance.SendMessage(subject, body, to, answerId);
            await Navigation.PopAsync();
        }        
    }
}
