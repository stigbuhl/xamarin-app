﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection.Emit;
using System.Text;
using ViggoMenuDemo.Controller;
using ViggoMenuDemo.CustomPages;
using ViggoMenuDemo.Model.Message;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Messages
{
    public class MessagesPage : RightPanelPage
    {
        ObservableCollection<MessageDTO> mail = new ObservableCollection<MessageDTO>();
        int[] testList = new int[10];
        ListView listView;

        //DeviceDatabase db = new DeviceDatabase();

        public MessagesPage()
        {
            GetMessages();

            listView = new ListView()
            {
                ItemsSource = mail,
                HasUnevenRows = true,
                IsPullToRefreshEnabled = true,
                RefreshCommand = new Command((o => GetMessages())),


                ItemTemplate = new DataTemplate(() =>
                {

                    Label senderLabel = new Label();
                    senderLabel.SetBinding(Label.TextProperty, "Id");

                    Label subjectLabel = new Label();
                    subjectLabel.SetBinding(Label.TextProperty, "Headline");

                    Label answerLabel = new Label();
                    answerLabel.SetBinding(Label.TextProperty, "AnswerId");

                    Label dateLabel = new Label();
                    dateLabel.SetBinding(Label.TextProperty, "Timestamp");

                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            VerticalOptions = LayoutOptions.Center,
                            Spacing = 0,
                            Children =
                                {
                                senderLabel,
                                subjectLabel,
                                answerLabel,
                                dateLabel
                                },
                        }
                    };
                })
            };

            listView.ItemSelected += (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    return;
                }

                PageController.Instance.ChangeDetailPage(new SelectedMessage((MessageDTO)e.SelectedItem));

                ((ListView)sender).SelectedItem = null;
            };

            StackLayout stackLayout = new StackLayout
            {
                Padding = new Thickness(20, 20, 20, 20),
                VerticalOptions = LayoutOptions.StartAndExpand,

                Children =
                {
                    new Label { Text = $"{Title}", FontSize = 20 },
                    new Button { Text = "Send ny besked", Command = new Command( async o =>
                            await MessageController.Instance.SendMessage(DateTime.Now.ToString(), DateTime.Now.ToString(), "2233", "1404")) },
                    listView
                }
            };

            if (Device.OS == TargetPlatform.iOS)
            {
                stackLayout.Padding = new Thickness(20, 40, 20, 20);
            }

            FillLayout = stackLayout;
        }

        public async void GetMessages()
        {
            mail.Clear();
            foreach (var item in await MessageController.Instance.GetMessages())
            {
                mail.Add(item);
            }

            if (listView.IsRefreshing)
            {
                listView.EndRefresh();
            }
        }        
    }
}
