﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using ViggoMenuDemo.CustomPages;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Admin
{
    public class SystemPage : RightPanelPage
    {
        public SystemPage()
        {
            FillLayout = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}
