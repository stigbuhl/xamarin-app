﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using ViggoMenuDemo.CustomPages;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Admin
{
    public class Administrator : RightPanelPage
    {
        public Administrator()
        {
            FillLayout = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}
