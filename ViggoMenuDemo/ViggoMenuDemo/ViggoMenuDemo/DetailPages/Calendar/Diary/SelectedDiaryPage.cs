﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Controller;
using ViggoMenuDemo.CustomPages;
using ViggoMenuDemo.Model.Diary;
using ViggoMenuDemo.Model.User;
using ViggoMenuDemo.Model.User.Post;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Calendar.Diary
{
    class SelectedDiaryPage : RightPanelPage
    {
        Editor editor;

        public SelectedDiaryPage(DiaryDTO diary)
        {
            StackLayout stackLayout = new StackLayout
            {
                Children =
                {
                    new Label
                    {
                        Text = diary.Headline,
                        FontAttributes = FontAttributes.Bold,
                    },
                    new Label
                    {
                        Text = diary.Contents,
                    },
                }
            };

            StackLayout usersStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal
            };

            if (diary.Users != null)
            {
                int counter = 0;

                foreach (var user in diary.Users)
                {
                    if (counter < 4)
                    {
                        usersStackLayout.Children.Add(DiaryView(user));
                    }
                    counter++;
                }

                if (counter > 1)
                {
                    usersStackLayout.Children.Add(new Label
                    {
                        VerticalOptions = LayoutOptions.Center,
                        FontAttributes = FontAttributes.Bold,
                        Text = $"Plus {counter - 4} andre",
                    });
                }
            }

            stackLayout.Children.Add(usersStackLayout);

            stackLayout.Children.Add(new Label
            {
                Text = "Kommentar",
            });

            editor = new Editor
            {
                HeightRequest = 100,
            };

            stackLayout.Children.Add(editor);

            stackLayout.Children.Add(new Button
            {
                BackgroundColor = Color.Green,
                Text = "Opret",
                Command = new Command(async o =>
                {
                    if (editor.Text != null)
                    {
                        int x = await DiaryController.Instance.PostComment(new PostAnswerDTO
                        {
                            Content = editor.Text,
                            PostId = diary.Id,
                        });

                        if (x == 1)
                        {
                            await Navigation.PopAsync();
                        }
                    }
                    else
                    {
                        await DisplayAlert("Indtast", "Indtast kommentar", "Ok");
                    }
                }),
            });

            FillLayout = stackLayout;
        }

        private StackLayout DiaryView(UserDTO user)
        {
            StackLayout stacklayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.Center,

                Children =
                    {
                        new Image
                        {
                            HeightRequest = 35,
                            WidthRequest = 35,
                            Aspect = Aspect.AspectFill,
                            HorizontalOptions = LayoutOptions.Center,
                            VerticalOptions = LayoutOptions.Center,
                            Source = user.ImageBig,
                        },
                        new Label
                        {
                            Text = user.FullName,
                            VerticalOptions = LayoutOptions.Center,
                        },
                    }
            };

            return stacklayout;
        }
    }
}
