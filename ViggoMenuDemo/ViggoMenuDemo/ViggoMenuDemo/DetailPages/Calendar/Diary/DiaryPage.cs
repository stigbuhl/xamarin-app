﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Controller;
using ViggoMenuDemo.CustomCells.Start;
using ViggoMenuDemo.CustomPages;
using ViggoMenuDemo.DetailPages.Calendar.Diary;
using ViggoMenuDemo.Model.Diary;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Calendar
{
    public class DiaryPage : RightPanelPage
    {
        List<DiaryDTO> diaryList;
        ObservableCollection<DiaryDTO> diaryItems;
        ObservableCollection<DiaryCategoryDTO> diaryCategories;
        StackLayout categoryStackLayout;

        public DiaryPage()
        {
            diaryList = new List<DiaryDTO>();
            diaryItems = new ObservableCollection<DiaryDTO>();
            DatePicker datePicker = new DatePicker();

            categoryStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,

                Children =
                {
                    
                }
            };

            ReadPage();

            ListView listView = new ListView
            {
                HasUnevenRows = true,
                IsPullToRefreshEnabled = true,

                ItemsSource = diaryItems,
                ItemTemplate = new DataTemplate(typeof(DiaryViewCell)),
            };

            listView.ItemSelected += (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    return;
                }

                if (e.SelectedItem is DiaryDTO)
                {
                    PageController.Instance.ChangeDetailPage(new SelectedDiaryPage((DiaryDTO)e.SelectedItem));
                }

                ((ListView)sender).SelectedItem = null;
            };

            StackLayout stackLayout = new StackLayout
            {
                BackgroundColor = Color.Silver,

                Children = {
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,

                        Children =
                        {
                            new Image
                            {
                                VerticalOptions = LayoutOptions.Center,
                                Source = "diary.png",
                                HeightRequest = 60,
                                WidthRequest = 60,
                            },
                            new Label
                            {
                                VerticalOptions = LayoutOptions.Center,
                                FontSize = 30,
                                FontAttributes = FontAttributes.Bold,
                                Text = "Dagbog",
                            }
                        },
                    },
                }
            };   

            StackLayout diaryStackLayout = new StackLayout
            {
                BackgroundColor = Color.White,

                Children =
                {
                    datePicker,
                    new Label
                    {
                        Text = "Vis kategorier:",
                    }
                }
            };

            diaryStackLayout.Children.Add(categoryStackLayout);
            diaryStackLayout.Children.Add(listView);

            stackLayout.Children.Add(diaryStackLayout);

            FillLayout = stackLayout;              
        }

        private async void ReadPage()
        {
            GetCategories();
            ReadDiary();
        }

        private async void ReadDiary()
        {
            List<DiaryDTO> newDiaryList = await DiaryController.Instance.GetDiaryList(DateTime.Now);

            foreach (var item in newDiaryList)
            {
                diaryItems.Add(item);
                diaryList.Add(item);
            }
        }

        private async void GetCategories()
        {
            categoryStackLayout.Children.Add(new Button
            {
                Text = "Ingen kategori",
                Command = new Command(o =>
                {
                    UpDateListView("Ingen kategori");
                })
            });

            foreach (var item in await DiaryController.Instance.GetCategories())
            {
                categoryStackLayout.Children.Add(new Button
                {
                    Text = item.Name,
                    Command = new Command(o=>
                    {
                        UpDateListView(item.Name);
                    })
                });            
            }
        }

        private void UpDateListView(string categoryName)
        {
            diaryItems.Clear();

            foreach (var item in diaryList)
            {
                if (item.Category.Count > 0)
                {
                    foreach (var category in item.Category)
                    {
                        if (category.Name == categoryName)
                        {
                            diaryItems.Add(item);
                        }
                    }
                }
                else if (categoryName == "Ingen kategori")
                {
                    diaryItems.Add(item);
                }
            }
        }
    }
}
