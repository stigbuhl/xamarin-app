﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.CustomCells.Start;
using ViggoMenuDemo.CustomCells.Start.Absence;
using ViggoMenuDemo.CustomCells.Start.Bulletin;
using ViggoMenuDemo.CustomCells.Start.Relations;
using ViggoMenuDemo.CustomCells.Start.Rooms;
using ViggoMenuDemo.Model.Absence;
using ViggoMenuDemo.Model.Bulletin;
using ViggoMenuDemo.Model.Diary;
using ViggoMenuDemo.Model.Home;
using ViggoMenuDemo.Model.Message;
using ViggoMenuDemo.Model.Room;
using ViggoMenuDemo.Model.User;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Start
{
    public class StartPageDataTemplateSelector : DataTemplateSelector
    {
        DataTemplate messageTemplate;
        DataTemplate diaryTemplate;
        DataTemplate roomTemplate;
        DataTemplate absenceTemplate;
        DataTemplate absenceSubHeaderTemplate;
        DataTemplate bulletinTemplate;
        DataTemplate relationsTemplate;

        DataTemplate testTemplate;

        public StartPageDataTemplateSelector()
        {            
            messageTemplate = new DataTemplate(typeof(MessageViewCell));
            diaryTemplate = new DataTemplate(typeof(DiaryViewCell));
            roomTemplate = new DataTemplate(typeof(RoomViewCell));
            absenceTemplate = new DataTemplate(typeof(AbsenceViewCell));
            absenceSubHeaderTemplate = new DataTemplate(typeof(AbsenceSubHeader));
            bulletinTemplate = new DataTemplate(typeof(BulletinViewCell));
            relationsTemplate = new DataTemplate(typeof(RelationViewCell));

            testTemplate = new DataTemplate(typeof(TestViewCell));
        }

        
        

        //Gets DataTemplate depending on object type
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item is MessageDTO)
            {
                return messageTemplate;
            }
            else if (item is DiaryDTO)
            {
                return diaryTemplate;
            }
            else if (item is RoomHomeDTO)
            {
                return roomTemplate;
            }
            else if (item is AbsenceDTO)
            {
                return absenceTemplate;
            }
            else if (item is AbsencePlaceDTO)
            {
                return absenceSubHeaderTemplate;
            }
            else if (item is BulletinDTO)
            {
                return bulletinTemplate;
            }
            else if (item is HomeRelationsDTO)
            {
                return relationsTemplate;
            }
            else
            {
                return testTemplate;
            }
        }
    }
}
