﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Controller;
using ViggoMenuDemo.CustomCells.Start;
using ViggoMenuDemo.DetailPages.Calendar.Diary;
using ViggoMenuDemo.DetailPages.Messages;
using ViggoMenuDemo.DetailPages.Start;
using ViggoMenuDemo.Model;
using ViggoMenuDemo.Model.Absence;
using ViggoMenuDemo.Model.Bulletin;
using ViggoMenuDemo.Model.Diary;
using ViggoMenuDemo.Model.Home;
using ViggoMenuDemo.Model.Interfaces;
using ViggoMenuDemo.Model.Message;
using ViggoMenuDemo.Model.Room;
using Xamarin.Forms;
using ViggoMenuDemo.CustomPages;

namespace ViggoMenuDemo.DetailPages
{
    public class StartPage : RightPanelPage
    {
        ObservableCollection<GroupedStartModel> grouped = new ObservableCollection<GroupedStartModel>();
        ListView listView;


        public StartPage()
        {
            grouped = new ObservableCollection<GroupedStartModel>();
            GetContent();
            NavigationPage.SetHasBackButton(this, false);
            StackLayout stackLayout = new StackLayout
            {
                Padding = new Thickness(20, 20, 20, 20),
            };

            listView = new ListView
            {
                ItemsSource = grouped,
                HasUnevenRows = true,
                IsPullToRefreshEnabled = true,
                RefreshCommand = new Command((o =>
                {
                    UpdateList();
                })),

                IsGroupingEnabled = true,
                
                //Defines Datatemplate of GroupHeader                
                GroupHeaderTemplate = new DataTemplate(typeof(GroupHeaderViewCell)),

                //Defines which DataTemplate an object will use
                ItemTemplate = new StartPageDataTemplateSelector(),
            };

            //What happens if user press a listviewitem
            listView.ItemSelected += (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    return;
                }

                //MessageDTO
                if (e.SelectedItem is MessageDTO)
                {
                    PageController.Instance.ChangeDetailPage(new SelectedMessage((MessageDTO)e.SelectedItem));
                }
                //DiaryDTO
                else if (e.SelectedItem is DiaryDTO)
                {
                    PageController.Instance.ChangeDetailPage(new SelectedDiaryPage((DiaryDTO)e.SelectedItem));
                }

                ((ListView)sender).SelectedItem = null;
            };

            stackLayout.Children.Add(listView);

            FillLayout = stackLayout;
        }

        private async void UpdateList()
        {

            await GetContent();
        }
 
        private async Task GetContent()
        {
            List<TestClass> side1 = new List<TestClass>();

            JArray responseArray = await HomeController.Instance.ReadHomePage();
            foreach (var item in responseArray)
            {
                side1.Add(new TestClass(item));
            }

            side1 = side1.OrderBy(o => o.Side).ThenBy(o => o.OrderBy).ToList();

            foreach (var item in side1)
            {
                AddingSwitch(item);
            }
        }
                
        private async void AddingSwitch(TestClass item)
        {
            switch (item.Functionid)
            {
                case 1:
                    await Message(item);
                    break;
                case 2:
                    await Rooms(item);
                    break;
                case 3:
                    Schedule(item);
                    break;
                case 5:
                    await Absence(item);
                    break;
                case 6:
                    await Relations(item);
                    break;
                case 20:
                    await Bulletin(item);
                    break;
                case 32:
                    await Diary(item);
                    break;
                case 165:
                    Injury(item);
                    break;
                case 202:
                    ExaminationsCensor(item);
                    break;
                default:
                    //GroupedStartModel groupedMisc = new GroupedStartModel { LongName = "Andet" };
                    //grouped.Add(groupedMisc);
                    //groupedMisc.Add(new TestClass { Text = item.ToString(), Functionid = item.Functionid });
                    break;
            }
        }

        private List<object> GroupAbsence(HomeAbsenceDTO homeAbsence)
        {
            List<int> absencePlaces = new List<int>();
            List<object> groupedAbsence = new List<object>();

            foreach (var absence in homeAbsence.AbsenceList)
            {
                if (!absencePlaces.Contains(absence.AbsencePlace.Id))
                {
                    groupedAbsence.Add(absence.AbsencePlace);
                    foreach (var item in homeAbsence.AbsenceList)
                    {
                        if (item.AbsencePlace.Id == absence.AbsencePlace.Id)
                        {
                            groupedAbsence.Add(item);
                        }
                    }
                    absencePlaces.Add(absence.AbsencePlace.Id);
                }
            }

            return groupedAbsence;
        }

        private async Task Message(TestClass item)
        {
            GroupedStartModel groupedMessages = new GroupedStartModel { LongName = "Beskeder", Url = $"http://192.168.15.92:8080/Content/default/img/svg/mail.svg" };
            grouped.Add(groupedMessages);
            List<MessageDTO> messages = await HomeController.Instance.ReadMessages(item.Show);
            if (messages != null)
                foreach (var message in messages)
                    groupedMessages.Add(message);
        }

        private async Task Rooms(TestClass item)
        {
            GroupedStartModel groupedRooms = new GroupedStartModel { LongName = "Rum", Url = $"http://192.168.15.92:8080/Content/default/img/128x128/discussion_double.png" };
            grouped.Add(groupedRooms);
            List<RoomHomeDTO> rooms = await HomeController.Instance.ReadRooms(item.Show);
            if (rooms != null)
                foreach (var room in rooms)
                    groupedRooms.Add(room);
        }

        private void Schedule(TestClass item)
        {
            GroupedStartModel groupedSchedule = new GroupedStartModel { LongName = "Dine begivenheder", Url = $"http://192.168.15.92:8080/Content/default/img/128x128/calendar.png" };
            grouped.Add(groupedSchedule);
            groupedSchedule.Add(new TestClass { Text = item.ToString(), Functionid = item.Functionid });
        }

        private async Task Absence(TestClass item)
        {
            GroupedStartModel groupedAbsence = new GroupedStartModel { LongName = "Fravær", Url = $"http://192.168.15.92:8080/Content/default/img/128x128/absence.png" };
            grouped.Add(groupedAbsence);
            HomeAbsenceDTO homeAbsence = await HomeController.Instance.ReadAbsence(item.Show);
            if (homeAbsence != null)
                foreach (var element in GroupAbsence(homeAbsence))
                {
                    groupedAbsence.Add(element);
                }
        }

        private async Task Relations(TestClass item)
        {
            GroupedStartModel groupedRelations = new GroupedStartModel { LongName = "Relationer", Url = $"http://192.168.15.92:8080/Content/default/img/128x128/user_blue.png" };
            grouped.Add(groupedRelations);
            List<HomeRelationsDTO> homeRelations = await HomeController.Instance.ReadRelations(item.Show);
            if (homeRelations != null)
                foreach (var relation in homeRelations)
                {
                    groupedRelations.Add(relation);
                }
        }

        private async Task Bulletin(TestClass item)
        {
            BulletinBoardDTO bulletinBoard = await HomeController.Instance.ReadBulletin(item.UserType, item.Show);
            GroupedStartModel groupedBulletin = new GroupedStartModel { LongName = $"Opslagtavle - {bulletinBoard.UserTypeName}", Url = $"http://192.168.15.92:8080/Content/default/img/128x128/board.png" };
            grouped.Add(groupedBulletin);
            if (bulletinBoard != null)
                foreach (var bulletinItem in bulletinBoard.BulletinList)
                    groupedBulletin.Add(bulletinItem);
        }

        private async Task Diary(TestClass item)
        {
            GroupedStartModel groupedDiary = new GroupedStartModel { LongName = "Dagbog", Url = $"http://192.168.15.92:8080/Content/default/img/128x128/diary.png" };
            grouped.Add(groupedDiary);
            List<DiaryDTO> diaries = await HomeController.Instance.ReadDiary(item.Show);
            if (diaries != null)
                foreach (var diary in diaries)
                    groupedDiary.Add(diary);
        }

        private void Injury(TestClass item)
        {
            GroupedStartModel groupedInjury = new GroupedStartModel { LongName = "Skader", Url = $"http://192.168.15.102:8080/Content/default/img/128x128/injury.png" };
            grouped.Add(groupedInjury);
            groupedInjury.Add(new TestClass { Text = item.ToString(), Functionid = item.Functionid });
        }

        private void ExaminationsCensor(TestClass item)
        {
            GroupedStartModel groupedExaminationCensor = new GroupedStartModel { LongName = "Censor", Url = $"http://192.168.15.102:8080/Content/default/img/128x128/user_blue.png" };
            grouped.Add(groupedExaminationCensor);
            groupedExaminationCensor.Add(new TestClass { Text = item.ToString(), Functionid = item.Functionid });
        }


    }
}
