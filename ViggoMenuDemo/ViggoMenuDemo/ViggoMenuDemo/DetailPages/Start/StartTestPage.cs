﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Controller;
using ViggoMenuDemo.CustomCells.Start;
using ViggoMenuDemo.Model;
using ViggoMenuDemo.Model.Bulletin;
using ViggoMenuDemo.Model.Diary;
using ViggoMenuDemo.Model.Home;
using ViggoMenuDemo.Model.Message;
using ViggoMenuDemo.Model.Room;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Start
{
    class StartTestPage : ContentPage
    {
        ObservableCollection<GroupedStartModel> grouped { get; set; }
        StackLayout stackLayout;

        public StartTestPage()
        {
            grouped = new ObservableCollection<GroupedStartModel>();

            NavigationPage.SetHasBackButton(this, false);

            ScrollView scrollView = new ScrollView();

            stackLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.StartAndExpand,
                Padding = new Thickness(20, 20, 20, 20),
            };

            scrollView.Content = stackLayout;

            GetContent();

            //ListView listView = new ListView
            //{
            //    ItemsSource = grouped,
            //    HasUnevenRows = true,

            //    IsGroupingEnabled = true,
            //    GroupHeaderTemplate = new DataTemplate(typeof(GroupHeaderViewCell)),

            //    ItemTemplate = new StartPageDataTemplateSelector(),
            //};
            //stackLayout.Children.Add(listView);

            Content = scrollView;
        }

        private async Task GetContent()
        {
            List<TestClass> side1 = new List<TestClass>();
            List<TestClass> side2 = new List<TestClass>();

            JArray responseArray = await HomeController.Instance.ReadHomePage();

            foreach (var item in responseArray)
            {
                if ((int)item["Side"] == 1)
                {
                    side1.Add(new TestClass(item));
                }
                else if ((int)item["Side"] == 2)
                {
                    side2.Add(new TestClass(item));
                }
            }

            side1 = side1.OrderBy(o => o.OrderBy).ToList();
            side2 = side2.OrderBy(o => o.OrderBy).ToList();

            foreach (var item in side1)
            {
                AddingSwitch(item);
            }

            foreach (var item in side2)
            {
                AddingSwitch(item);
            }
        }

        private async void AddingSwitch(TestClass item)
        {
            switch (item.Functionid)
            {
                case 1:
                    await Message(item);
                    break;
                case 2:
                    await Rooms(item);
                    break;
                //case 3:
                //    Schedule(item);
                //    break;
                //case 5:
                //    Absence(item);
                //    break;
                //case 6:
                //    Relations(item);
                //    break;
                //case 20:
                //    Bulletin(item);
                //    break;
                //case 32:
                //    Diary(item);
                //    break;
                //case 165:
                //    Injury(item);
                //    break;
                //case 202:
                //    ExaminationsCensor(item);
                //    break;
                default:
                    //GroupedStartModel groupedMisc = new GroupedStartModel { LongName = "Andet" };
                    //grouped.Add(groupedMisc);
                    //groupedMisc.Add(new TestClass { Text = item.ToString(), Functionid = item.Functionid });
                    break;
            }
        }

        private List<object> GroupAbsence(HomeAbsenceDTO homeAbsence)
        {
            List<int> absencePlaces = new List<int>();
            List<object> groupedAbsence = new List<object>();

            foreach (var absence in homeAbsence.AbsenceList)
            {
                if (!absencePlaces.Contains(absence.AbsencePlace.Id))
                {
                    groupedAbsence.Add(absence.AbsencePlace);
                    foreach (var item in homeAbsence.AbsenceList)
                    {
                        if (item.AbsencePlace.Id == absence.AbsencePlace.Id)
                        {
                            groupedAbsence.Add(item);
                        }
                    }
                    absencePlaces.Add(absence.AbsencePlace.Id);
                }
            }

            return groupedAbsence;
        }

        private async Task Message(TestClass item)
        {
            //stackLayout.Children.Add(GroupHeader("Beskeder", $"https://demotest.viggo.dk/Content/default/img/128x128/mail.png"));

            List<GroupedStartModel> grouped = new List<GroupedStartModel>();

            GroupedStartModel groupedMessages = new GroupedStartModel { LongName = "Beskeder", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/mail.png" };

            grouped.Add(groupedMessages);

            List<MessageDTO> messages = await HomeController.Instance.ReadMessages(item.Show);

            foreach (var message in messages)
            {
                groupedMessages.Add(message);
            }

            stackLayout.Children.Add(new ListView
            {             
                HasUnevenRows = true,
                ItemsSource = grouped,
                IsGroupingEnabled = true,
                GroupHeaderTemplate = new DataTemplate(typeof(GroupHeaderViewCell)),
                ItemTemplate = new StartPageDataTemplateSelector(),
            });

            //GroupedStartModel groupedMessages = new GroupedStartModel { LongName = "Beskeder", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/mail.png" };
            //grouped.Add(groupedMessages);
            //List<MessageDTO> messages = await HomeController.Instance.ReadMessages(item.Show);
            //if (messages != null)
            //    foreach (var message in messages)
            //        groupedMessages.Add(message);
        }

        private async Task Rooms(TestClass item)
        {
            //stackLayout.Children.Add(GroupHeader("Rum", $"https://demotest.viggo.dk/Content/default/img/128x128/discussion_double.png"));

            List<GroupedStartModel> grouped = new List<GroupedStartModel>();

            GroupedStartModel groupedRooms = new GroupedStartModel { LongName = "Rum", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/discussion_double.png" };

            grouped.Add(groupedRooms);

            List<RoomHomeDTO> rooms = await HomeController.Instance.ReadRooms(item.Show);

            foreach (var room in rooms)
            {
                groupedRooms.Add(room);
            }

            stackLayout.Children.Add(new ListView
            {
                HasUnevenRows = true,
                ItemsSource = grouped,
                IsGroupingEnabled = true,
                GroupHeaderTemplate = new DataTemplate(typeof(GroupHeaderViewCell)),
                ItemTemplate = new StartPageDataTemplateSelector(),
            });

            //GroupedStartModel groupedRooms = new GroupedStartModel { LongName = "Rum", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/discussion_double.png" };
            //grouped.Add(groupedRooms);
            //List<RoomHomeDTO> rooms = await HomeController.Instance.ReadRooms(item.Show);
            //if (rooms != null)
            //    foreach (var room in rooms)
            //        groupedRooms.Add(room);
        }

        private async void Schedule(TestClass item)
        {
            GroupedStartModel groupedSchedule = new GroupedStartModel { LongName = "Dine begivenheder", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/calendar.png" };
            grouped.Add(groupedSchedule);
            groupedSchedule.Add(new TestClass { Text = item.ToString(), Functionid = item.Functionid });
        }

        private async void Absence(TestClass item)
        {
            GroupedStartModel groupedAbsence = new GroupedStartModel { LongName = "Fravær", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/absence.png" };
            grouped.Add(groupedAbsence);
            HomeAbsenceDTO homeAbsence = await HomeController.Instance.ReadAbsence(item.Show);
            if (homeAbsence != null)
                foreach (var element in GroupAbsence(homeAbsence))
                {
                    groupedAbsence.Add(element);
                }
        }

        private async void Relations(TestClass item)
        {
            GroupedStartModel groupedRelations = new GroupedStartModel { LongName = "Relationer", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/user_blue.png" };
            grouped.Add(groupedRelations);
            List<HomeRelationsDTO> homeRelations = await HomeController.Instance.ReadRelations(item.Show);
            if (homeRelations != null)
                foreach (var relation in homeRelations)
                {
                    groupedRelations.Add(relation);
                }
        }

        private async void Bulletin(TestClass item)
        {
            BulletinBoardDTO bulletinBoard = await HomeController.Instance.ReadBulletin(item.UserType, item.Show);
            GroupedStartModel groupedBulletin = new GroupedStartModel { LongName = $"Opslagtavle - {bulletinBoard.UserTypeName}", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/board.png" };
            grouped.Add(groupedBulletin);
            if (bulletinBoard != null)
                foreach (var bulletinItem in bulletinBoard.BulletinList)
                    groupedBulletin.Add(bulletinItem);
        }

        private async void Diary(TestClass item)
        {
            GroupedStartModel groupedDiary = new GroupedStartModel { LongName = "Dagbog", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/diary.png" };
            grouped.Add(groupedDiary);
            List<DiaryDTO> diaries = await HomeController.Instance.ReadDiary(item.Show);
            if (diaries != null)
                foreach (var diary in diaries)
                    groupedDiary.Add(diary);
        }

        private async void Injury(TestClass item)
        {
            GroupedStartModel groupedInjury = new GroupedStartModel { LongName = "Skader", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/injury.png" };
            grouped.Add(groupedInjury);
            groupedInjury.Add(new TestClass { Text = item.ToString(), Functionid = item.Functionid });
        }

        private async void ExaminationsCensor(TestClass item)
        {
            GroupedStartModel groupedExaminationCensor = new GroupedStartModel { LongName = "Censor", Url = $"https://demotest.viggo.dk/Content/default/img/128x128/user_blue.png" };
            grouped.Add(groupedExaminationCensor);
            groupedExaminationCensor.Add(new TestClass { Text = item.ToString(), Functionid = item.Functionid });
        }

        private StackLayout GroupHeader(string longName, string imageUrl)
        {
            var stackLayout = new StackLayout
            {
                Margin = 0,
                Spacing = 0,
                //HeightRequest = 120,
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Transparent,

                Children =
                {
                    new Image
                    {
                        VerticalOptions = LayoutOptions.Center,
                        Source = new UriImageSource
                        {
                            Uri = new Uri(imageUrl),
                        }
                    },
                    new Label
                    {
                        VerticalOptions = LayoutOptions.Center,
                        FontAttributes = FontAttributes.Bold,
                        FontSize = 30,
                        Text = longName,
                    },
                },
            };

            return stackLayout;
        }
    }
}
