﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViggoMenuDemo.DetailPages.Start
{
    class GroupedStartModel : ObservableCollection<object>
    {
        public string LongName { get; set; }
        public string ShortName { get; set; }
        public string Url { get; set; }
    }
}
