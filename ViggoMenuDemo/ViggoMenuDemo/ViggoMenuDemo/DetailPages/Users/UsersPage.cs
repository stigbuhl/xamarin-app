﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using ViggoMenuDemo.Controller;
using ViggoMenuDemo.CustomCells.UsersPage;
using ViggoMenuDemo.CustomPages;
using ViggoMenuDemo.Model.User;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Users
{
    public class UsersPage : RightPanelPage
    {
        ObservableCollection<UserDTO> users;
        ListView listView;
        
        public UsersPage(JToken token)
        {
            users = new ObservableCollection<UserDTO>();

            GetUserList((int)token["Id"]);            

            listView = new ListView
            {
                ItemsSource = users,
                HasUnevenRows = true,
                IsPullToRefreshEnabled = true,
                RefreshCommand = new Command((async o => await GetUserList((int)token["Id"]))),

                ItemTemplate = new DataTemplate(typeof(UserViewCell)),
            };

            FillLayout = new StackLayout
            {
                Children = {
                    listView
                }
            };
        }

        //Populates "users"
        private async Task GetUserList(int id)
        {
            users.Clear();

            foreach (var item in await UsersController.Instance.GetUserListFromId(id))
            {
                users.Add(item);
            }

            if (listView.IsRefreshing)
            {
                if (listView.IsRefreshing)
                {
                    listView.EndRefresh();
                }
            }
        }
    }
}
