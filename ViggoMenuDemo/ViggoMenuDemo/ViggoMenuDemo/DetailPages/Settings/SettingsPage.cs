﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using ViggoMenuDemo.CustomPages;
using Xamarin.Forms;

namespace ViggoMenuDemo.DetailPages.Settings
{
    public class SettingsPage : RightPanelPage
    {
        public SettingsPage()
        {
            NavigationPage.SetHasBackButton(this, false);
            StackLayout stackLayout = new StackLayout
            {
                Padding = new Thickness(20, 20, 20, 20),
                BackgroundColor = Color.Gray,
                Children =
                {
                    new Label { Text = "serif", FontFamily = "serif", FontSize = 30, },
                    new Label { Text = "monospace", FontFamily = "monospace", FontSize = 30, },
                    new Label { Text = "bold monospace", FontFamily = "monospace", FontAttributes = FontAttributes.Bold, FontSize = 30 },
                    new Label { Text = "italic monospace", FontFamily = "monospace", FontAttributes = FontAttributes.Italic, FontSize = 30 },
                    new Label { Text = "italic + bold monospace", FontFamily = "monospace", FontAttributes = FontAttributes.Italic | FontAttributes.Bold, FontSize = 30 }
                }
            };

            FillLayout = stackLayout;
        }
    }
}
